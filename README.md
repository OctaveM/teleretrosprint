# Guide d’installation

Ce guide détaille les étapes pour déployer le projet. La documentation du projet est disponible dans le dossier documentation.

## Installation de la base de données
Le script d’installation src_BDD.sql se situe à la racine de l’archive et créé une base de donnée MySQL. Vous devez avoir MySQL installé.
Lancez MySQL depuis la racine du projet et dans le shell MySQL, appelez le script avec la commande :
```
source src_BDD.SQL
```

La base de données sera créée à l’issue de cette étape. Le script est détaillé dans la section Base de données du Guide Back-end.

## Installation du back-end
Pour lancer le back-end, maven est nécessaire. Placez vous dans le dossier back-end et exécutez les commandes suivantes :
```
mvn compile
mvn exec:java -Dexec.mainClass=teleretro.App
```

La première commande compile les sources fournies et la deuxième lance le serveur sur le port 8080.

## Installation du front-end
Pour lancer le front-end, node.js est nécessaire. Placez vous dans le dossier front-end et exécutez les commandes suivantes :
```
npm install
npm start
```

La première commande installe les package nécessaires au fonctionnement du front et la deuxième lance le serveur sur le port 4200.

Une fois ces 3 étapes effectuées, vous pourrez accéder au site depuis votre machine à l’adresse localhost:4200
