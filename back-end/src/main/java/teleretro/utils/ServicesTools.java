package teleretro.utils;


import org.json.simple.JSONObject;

public class ServicesTools {

	public static JSONObject serviceRefused(String message){
		JSONObject json = new JSONObject();
		json.put("status", "KO");
		json.put("message", message);
		return json;
	}
	
	public static JSONObject serviceAccepted(){
		JSONObject json = new JSONObject();
		json.put("status", "OK");
		return json;
	}
	
	public static JSONObject serviceNotYetImplemented(String serviceName){
		JSONObject json = new JSONObject();
			json.put("status", "KO");
			json.put("message", "The service "+serviceName+" has not been implemented yet");
			return json;
	}
	
}
