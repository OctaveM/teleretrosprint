package teleretro.utils;

import java.math.BigInteger;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Random;

public class PasswordUtils {
	private static String sample = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
	
	/*
	 * Génère un mot de passe de 20 caractères
	 */
	public static String generatePassword() {
		Random r = new Random();
		StringBuilder s = new StringBuilder();
		for (int i =0; i<20; i++)
			s.append(sample.charAt(r.nextInt(sample.length())));
		return s.toString();
	}
	/*
	 * Génère un sel de 5 caractères
	 */
	public static String generateSalt() {
		Random r = new Random();
		StringBuilder s = new StringBuilder();
		for (int i =0; i<5; i++)
			s.append(sample.charAt(r.nextInt(sample.length())));
		return s.toString();
	}
	
	//https://www.geeksforgeeks.org/sha-1-hash-in-java/
		public static String getHash(String password) {
			try {
				MessageDigest md = MessageDigest.getInstance("SHA-1");
				byte[] messageDigest = md.digest(password.getBytes());
				BigInteger no = new BigInteger(1, messageDigest);
				String hashtext = no.toString(16);
				
				while (hashtext.length() < 32) {
					hashtext = "0"+hashtext;
				}
				return hashtext;
			} catch (NoSuchAlgorithmException e) {
				throw new RuntimeException(e);
			}
		}
}
