package teleretro.utils;
import java.io.FileReader;
import java.io.IOException;
import java.sql.Timestamp;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;

import javax.mail.Message;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import teleretro.utils.exceptions.JSONParsingException;  
  

public class MailSender {
    
	private static String[] getMailingList() {
		org.hibernate.Session s = SessionUtil.getSession();
		try {
			return ((List<String>)s.createQuery("SELECT u.mail from Utilisateur as u WHERE u.activated is true").list()).toArray(new String[0]);
		} finally {
			s.close();
		}
	}
	
	public static String createBody(Map<String, String> vars, JSONArray body) throws JSONParsingException {
		StringBuilder sb = new StringBuilder();
		for (int i=0; i<body.size(); i++) {
			JSONObject tmp = (JSONObject) body.get(i);
			if (tmp.keySet().size()!=1) {
				throw new JSONParsingException("Le JSONObject contient un nombre de variable différent de 1 :"+tmp.toString());
			}
			if (tmp.containsKey("line")) {
				sb.append(tmp.get("line"));
				sb.append("<br/>");
			} else if (tmp.containsKey("string")) {
				sb.append(tmp.get("string"));
			} else if (tmp.containsKey("var")) {
				String tmpVar = vars.get(tmp.get("var"));
				if (tmpVar == null) {
					throw new JSONParsingException("La variable "+tmp.get("var")+ " n'existe pas.");
				}
				sb.append(tmpVar);
			} else {
				throw new JSONParsingException("Le mot clé utilisé n'existe pas : "+tmp.toString());
			}
		}
		return sb.toString();
	}
	public static Boolean sendEmail(String fileToParse, Map<String, String> env, String[] to)
	{
		String objetMail;
		JSONArray corp;
		//On parse le template
		JSONParser parserTemplate = new JSONParser();
		try {
			JSONObject template = (JSONObject) parserTemplate.parse(new FileReader(fileToParse));
			objetMail = (String) template.get("objet");
			corp = (JSONArray) template.get("corps");
			
			//On envoie
			JSONParser parserConfig = new JSONParser();

			JSONObject config = (JSONObject) parserConfig.parse(new FileReader("../mails/mail-config.json"));
			Properties props = System.getProperties();
			JSONObject smtp = (JSONObject) config.get("smtp");
			//charger les propriétées de smtp
			for(Object s :smtp.keySet()) {
				props.put("mail.smtp."+(String)s, smtp.get(s).toString());
			}
			
			Session session = Session.getDefaultInstance(props);
			
			MimeMessage msg = new MimeMessage(session);
			msg.setFrom(new InternetAddress((String)config.get("from"), (String)config.get("fromName")));
			//Récupérer les adresses de tous les personnes à qui envoyer le mail
			InternetAddress[] recipients = new InternetAddress[to.length];
			for (int i = 0; i < to.length; i++)
				recipients[i] = new InternetAddress(to[i]);
			msg.setRecipients(Message.RecipientType.TO, recipients);//TODO remplacer TO par BCC ?
			msg.setSubject(objetMail);
			msg.setContent(createBody(env, corp), "text/html");
			
			Transport transport = session.getTransport();
			
			transport.connect((String) smtp.get("host"), (String) config.get("from"), (String) config.get("password"));
			
			transport.sendMessage(msg, msg.getAllRecipients());
			
			transport.close();
			return true;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return false;
	}

		public static boolean sendMailCreationUser(String mail, String user, String password, boolean isMdc) {
			Map<String, String> map = new HashMap<>();
			map.put("mail", mail);
			map.put("pseudo", user);
			map.put("password", password);
			String[] dest = {mail};
			return sendEmail("../mails/mail-user-creation.json",map, dest);
		}
		public static boolean sendMailPasswordReset(String mail, String newPassword) {
			Map<String, String> map = new HashMap<>();
			map.put("mail", mail);
			map.put("password", newPassword);
			String[] dest = {mail};
			return sendEmail("../mails/mail-reset-password.json", map, dest);
		}
		public static boolean sendMailReactivationUser(String mail, String nickname) {
			Map<String, String> map = new HashMap<>();
			map.put("mail", mail);
			map.put("pseudo", nickname);
			String[] dest = {mail};
			return sendEmail("../mails/mail-user-reactivation.json", map, dest);
		}
		public static boolean sendMailDesactivationUser(String mail, String nickname) {
			Map<String, String> map = new HashMap<>();
			map.put("mail", mail);
			map.put("pseudo", nickname);
			String[] dest = {mail};
			return sendEmail("../mails/mail-user-desactivation.json", map, dest);
		}
		public static boolean sendMailCreationCeremony(String ceremony, Timestamp deadline) {
			Map<String, String> map = new HashMap<>();
			map.put("name", ceremony);
			map.put("deadline", deadline.toString());
			String[] dest = getMailingList();
			return sendEmail("../mails/mail-ceremony-creation.json", map, dest);
		}
		public static boolean sendMailDeadlineReminder() {
			Map<String, String> map = new HashMap<>();
			String[] dest = getMailingList();
			return sendEmail("../mails/mail-deadline-reminder.json", map, dest);
		}
}
