package teleretro.utils;

import java.sql.Timestamp;
import java.util.List;

import org.hibernate.Session;
import org.hibernate.query.Query;

import teleretro.beans.SessionSaver;

public class TokenUtils {

	private static long dureeValCle=2*60*60*1000L;//2 heures
	
	public static boolean isTokenValid(Session session, String token) {
		SessionSaver result = (SessionSaver) session.get(SessionSaver.class, token);
		if (result== null)
			return false;
		//vérifier si clée périmée
		// la date du token + sa duréee de validité est il plus petit que maintenant == clé pérmiée
		if (result.getSession_date().getTime()+dureeValCle<System.currentTimeMillis()) {
			session.beginTransaction();
			session.delete(result);
			session.getTransaction().commit();
			return false;
		}
		//on met à jour le token
		session.beginTransaction();
		result.setSession_date(new Timestamp(System.currentTimeMillis()));
		session.update(result);
		session.getTransaction().commit();
		return true;
	}
	
	public static boolean isPassWordValidFromToken(Session session, String token, String password) {
		Query query = session.createQuery("FROM SessionSaver as s WHERE s.token = :token");
		query.setParameter("token", token);
		SessionSaver result = (SessionSaver) query.uniqueResult();
		if (result==null)
			return false;
		if (result.getUser().getPassword().equals(PasswordUtils.getHash(password+result.getUser().getSalt())))//on vérifie que le mot de passe fourni est bien celui de l'utilisateur
			return true;
		return false;
	}

	public static boolean isMDCFromToken(Session session, String token) {
			Query query = session.createQuery("FROM SessionSaver as s WHERE s.token = :token");
			query.setParameter("token", token);
			SessionSaver result = (SessionSaver) query.uniqueResult();
			if (result==null)
				return false;
			if (result.getUser().isMaster())//on vérifie que c'est un master
				return true;
			return false;
	}
}
