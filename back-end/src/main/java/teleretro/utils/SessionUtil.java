package teleretro.utils;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

public class SessionUtil {

    private static SessionFactory sessionFactory;   
    
    public static Session getSession(){
    	if (sessionFactory == null)
    		sessionFactory = new Configuration().configure().buildSessionFactory();
        return sessionFactory.openSession();
    }
    
    public static void closeSessionFactory() {
    	if(sessionFactory != null) {
    		sessionFactory.close();
    	}
    }
}
