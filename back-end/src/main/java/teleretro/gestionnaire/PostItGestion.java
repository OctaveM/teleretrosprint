package teleretro.gestionnaire;


import java.sql.Date;
import java.util.List;
import org.hibernate.Session;
import org.hibernate.query.Query;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

import teleretro.beans.Ceremony;
import teleretro.beans.PostIt;
import teleretro.beans.SessionSaver;
import teleretro.beans.Utilisateur;
import teleretro.gestionnaire.exceptions.DidntStartException;
import teleretro.gestionnaire.exceptions.UtilisateurIsNotParticipatingException;
import teleretro.utils.ServicesTools;
import teleretro.utils.SessionUtil;
import teleretro.utils.TokenUtils;

public class PostItGestion {

	/** permet d'ajouter deux post-its (un positif, un négatif).
	 * 
	 * @param token
	 * @param positif
	 * @param negatif
	 * @return
	 */
	public static JSONObject insert(String token, String positif, String negatif) {
		
		Session session = SessionUtil.getSession();
		
		//vérifie que le token est valide
		if(!TokenUtils.isTokenValid(session, token)) {
			return ServicesTools.serviceRefused("token invalide");
		}
		
		//vérifie qu'on est dans une post-cérémonie
		Query query = session.createQuery("FROM Ceremony as c WHERE c.ceremonyOver is false");
		
		try {
			Ceremony c = (Ceremony)query.uniqueResult();
			
			if (c == null) {
				return ServicesTools.serviceRefused("aucune post-ceremonie en cours.");
			}
			//vérifier que la cérém est pas encore lancée / deadline pas encore passée

			//vérifie que le contenu est non-null
			if(positif == null || negatif == null ) {
				return ServicesTools.serviceRefused("Le contenu des post-its doit être non nul.");
			}
			
			//vérifie que le contenu est plus petit que 1024
			if(positif.length() > 1024 || negatif.length() > 1024 ) {
				return ServicesTools.serviceRefused("Le contenu des post-its doit être inférieur à 1024 caractères (positif : "+positif.length()+" ; negatif : "+negatif.length()+").");
			}
			
			//vérifier que le contenu est non-vide (ne contiens pas que des espaces) 
			if(positif.trim().isEmpty() || negatif.trim().isEmpty()) {
				return ServicesTools.serviceRefused("Le contenu des post-it ne doit pas être vide");
			}
			
			//recupérer user a partir du token
			Utilisateur u = ((SessionSaver) session.load(SessionSaver.class, token)).getUser();
			
			//vérifier que l'utilisateur n'a pas déja des postits
			query = session.createQuery("FROM PostIt as p WHERE p.user = :u "
			+ "AND p.ceremony= :c");
			query.setParameter("u", u);
			query.setParameter("c", c);
			List<PostIt> postits = query.list();
			if (postits.size() > 0) {
				return ServicesTools.serviceRefused("vos avez déja insérer des postits, veuillez "
						+ "utilisez la fonctionnalité 'modifier ses postits'");
			}
			
			//Insertion des post-its
			JSONObject res = ServicesTools.serviceAccepted();
			session.beginTransaction();
			//insertion postit positif
			PostIt p1 = new PostIt();
			p1.setCeremony(c);
			p1.setContent(positif);
			p1.setType(true);
			p1.setUser(u);
			session.save(p1);
			session.flush();
	        session.clear();
			//insertion postit négatif
			PostIt p2 = new PostIt();
			p2.setCeremony(c);
			p2.setContent(negatif);
			p2.setType(false);
			p2.setUser(u);
			session.save(p2);
			session.getTransaction().commit();
			return res;
			
		} finally {
			session.close();
		}
	}
	/**
	 * permet de modifier deux post-its
	 * @param token
	 * @param positif
	 * @param negatif
	 * @return
	 */
	public static JSONObject update(String token, String positif, String negatif) {
		
		Session session = SessionUtil.getSession();
		
		//vérifie que le token est valide
		if(!TokenUtils.isTokenValid(session, token)) {
			System.out.println(token);
			return ServicesTools.serviceRefused("token invalide");
		}
		
		//vérifie qu'on est dans une post-cérémonie
		Query query = session.createQuery("FROM Ceremony as c WHERE c.ceremonyOver is false");
		
		try {
			Ceremony c = (Ceremony)query.uniqueResult();
			
			if (c == null) {
				return ServicesTools.serviceRefused("aucune post-ceremonie en cours.");
			}

			//vérifie que le contenu est non-null
			if(positif == null || negatif == null ) {
				return ServicesTools.serviceRefused("Le contenu des post-its doit être non nul.");
			}
			
			//vérifie que le contenu est plus petit que 1024
			if(positif.length() > 1024 || negatif.length() > 1024 ) {
				return ServicesTools.serviceRefused("Le contenu des post-its doit être inférieur à 1024 caractères (positif : "+positif.length()+" ; negatif : "+negatif.length()+").");
			}
			
			//vérifier que le contenu est non-vide (ne contiens pas que des espaces) 
			if(positif.trim().isEmpty() || negatif.trim().isEmpty()) {
				return ServicesTools.serviceRefused("le contenu des post-it ne doit pas être vide");
			}
			
			//recupérer user a partir du token
			Utilisateur u = ((SessionSaver) session.load(SessionSaver.class, token)).getUser();
			
			//vérifier que les deux postits de l'utilisateur existe
			query = session.createQuery("FROM PostIt as p WHERE p.user = :u "
			+ "AND p.ceremony= :c");
			query.setParameter("u", u);
			query.setParameter("c", c);
			List<PostIt> postits = query.list();
			if (postits.size() < 2) {
				return ServicesTools.serviceRefused("vous devez d'abord inserer vos postits avant de les modifier");
			}
			
			//Modification des post-its
			JSONObject res = ServicesTools.serviceAccepted();
			session.beginTransaction();
			for(PostIt p : postits) {
				if(p.isType()) {
					//update postit positif
					p.setContent(positif);
					session.update(p);
					session.flush();
			        session.clear();
				}else {
					//update postit negatif
					p.setContent(negatif);
					session.update(p);
					session.flush();
			        session.clear();
				}
			}
			session.getTransaction().commit();
			return res;
			
		} finally {
			session.close();
		}
	}
	
	
	/**
	 * permet de récupérer ses post-its de la cérémonie courante
	 * @param token
	 * @return
	 */
	public static JSONObject getOwnCurrent(String token) {

		Session session = SessionUtil.getSession();
		
		//vérifie que le token est valide
		if(!TokenUtils.isTokenValid(session, token)) {
			System.out.println(token);
			return ServicesTools.serviceRefused("token invalide");
		}
		
		//vérifie qu'on est dans une post-cérémonie
		Query query = session.createQuery("FROM Ceremony as c WHERE c.ceremonyOver is false");
		
		try {
			Ceremony c = (Ceremony)query.uniqueResult();
			
			if (c == null) {
				return ServicesTools.serviceRefused("aucune post-ceremonie en cours.");
			}

			//recupérer user a partir du token
			Utilisateur u = ((SessionSaver) session.load(SessionSaver.class, token)).getUser();
			
			//récupérer les post-its de l'utilisateur pour la cérémonie courante.
			query = session.createQuery("FROM PostIt as p WHERE p.user = :u "
			+ "AND p.ceremony= :c");
			query.setParameter("u", u);
			query.setParameter("c", c);
			List<PostIt> postits = query.list();
			if (postits.size() < 2) {
				return ServicesTools.serviceRefused("postits non insérés");
			}
			
			//renvoyer les postits
			JSONObject res = ServicesTools.serviceAccepted();
			
			for(PostIt p : postits) {
				if(p.isType()) {
					res.put("positif", p.getContent());
				}else {
					res.put("negatif", p.getContent());
				}
			}
			return res;
			
		} finally {
			session.close();
		}
	}
	
	/**
	 * supprimer les post-it de l'utilisateur pour la cérémonie courante
	 * @param token
	 * @return
	 */
	public static void deleteUserPostits(Utilisateur u, Session session) {
		
			//récuperer la cérémonie courante
			Query query = session.createQuery("FROM Ceremony as c WHERE c.ceremonyOver is false");
			//query.setParameter("date", new Date(System.currentTimeMillis()));
			Ceremony c = (Ceremony)query.uniqueResult();
			if (c != null) {
				//supprimer les post-its de l'utilisateur pour la cérémonie courante.
				query = session.createQuery("DELETE FROM PostIt as p WHERE ceremony = :c AND user = :u");
				query.setParameter("c", c);
				query.setParameter("u", u);
				int result = query.executeUpdate();
			}
		
	}
	public static JSONObject getPostItCerem(String token) {
		Session session = SessionUtil.getSession();
		// vérifier que le token est valide
		try {
			if (!TokenUtils.isTokenValid(session, token))
				return ServicesTools.serviceRefused("Token obsolète, veuillez vous connecter");
			JSONObject ret = ServicesTools.serviceAccepted();
			ret.put("postIts",
					CeremonyLive.getInstance().getMyPostIts(session.load(SessionSaver.class, token).getUser().getNickname())
					);
			return ret;
			
		} catch (InterruptedException e) {
			return ServicesTools.serviceRefused("Erreur java");
		} catch (DidntStartException e) {
			return ServicesTools.serviceRefused("L'appel au service de récupération des post-its a été effectué alors qu'aucune cérémonie n'a été lancée.");
		} catch (UtilisateurIsNotParticipatingException e) {
			return ServicesTools.serviceRefused("L'utilisateur ne participe pas à la cérémonie et ne peut donc pas récupérer de post-its");
		} finally {
			session.close();
		}
	}
	public static JSONObject getPostItFromCerem(String token, int idCerem) {
		Session session = SessionUtil.getSession();
		// vérifier que le token est valide
		try {
			if (!TokenUtils.isTokenValid(session, token))
				return ServicesTools.serviceRefused("Token obsolète, veuillez vous connecter");
			if (!TokenUtils.isMDCFromToken(session, token))
				return ServicesTools.serviceRefused("Seul un maître de cérémonie peut accéder aux post-its d'une cérémonie");
			Ceremony c = session.get(Ceremony.class, idCerem);
			if (c == null)
				return ServicesTools.serviceRefused("Aucune cérémonie trouvée avec cet id.");
			List<PostIt> postits =session.createQuery("FROM PostIt p WHERE p.ceremony = :c").setParameter("c", c).getResultList();
			JSONArray post = new JSONArray();
			for(PostIt p : postits) {
				JSONObject temp = new JSONObject();
				temp.put("author", p.getUser().getNickname());
				temp.put("positif", p.isType());
				temp.put("content", p.getContent());
				post.add(temp);
			}
			JSONObject ret = ServicesTools.serviceAccepted();
			ret.put("postits", post);
			return ret;
		} finally {
			session.close();
		}
	}
	
}
