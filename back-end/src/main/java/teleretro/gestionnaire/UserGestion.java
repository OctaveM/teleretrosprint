package teleretro.gestionnaire;

import java.sql.Timestamp;
import java.util.Date;
import java.util.List;
import java.util.UUID;

import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.exception.ConstraintViolationException;
import org.hibernate.query.Query;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

import teleretro.beans.Ceremony;
import teleretro.beans.SessionSaver;
import teleretro.beans.Utilisateur;
import teleretro.utils.MailSender;
import teleretro.utils.PasswordUtils;
import teleretro.utils.ServicesTools;
import teleretro.utils.SessionUtil;
import teleretro.utils.TokenUtils;


//REVOIR LE PROCESS DE SUPPRESSION 
public class UserGestion {
	
	public static JSONObject connect(String login, String password) {
		Session session = SessionUtil.getSession();
		Query query = session.createQuery("FROM Utilisateur as u WHERE (u.nickname = :login OR u.mail = :login)");
		query.setParameter("login", login);
		try {
			Utilisateur user = (Utilisateur) query.uniqueResult();
			
			// Il faut vérifier que le login ou mail et password sont bon.
			if (user == null) {
				return ServicesTools.serviceRefused("Impossible de se connecter : pas d'utilisateur trouvé avec ce login.");
			}
			

			if (!user.isActivated()) {
				return ServicesTools.serviceRefused("Impossible de se connecter : le compte a été désactivé.");
			}

			//Si c'est bon, on génère un token, on le stocke et on le renvoie
			if (user.getPassword().equals(PasswordUtils.getHash(password+user.getSalt()))) {
				JSONObject res = ServicesTools.serviceAccepted();
				Query q = session.createQuery("FROM SessionSaver as s WHERE s.user = :user");
				q.setParameter("user", user);
				SessionSaver s = (SessionSaver) q.uniqueResult();
				String token;
				if (s == null || !TokenUtils.isTokenValid(session, s.getToken())) {
					token = UUID.randomUUID().toString();
					session.beginTransaction();
					session.save(new SessionSaver(token, user, new Timestamp(System.currentTimeMillis())));
					session.getTransaction().commit();
				} else { 
					//TODO utiliser TokenUtils pour supprimer le token si il est obsolète et en créer un nouveau
					token = s.getToken();
				}
				res.put("token", token);
				if(TokenUtils.isMDCFromToken(session, token)) {
					res.put("mdc", true);
				}else {
					res.put("mdc", false);
				}
				return res;
			}
			return ServicesTools.serviceRefused("Impossible de se connecter : mot de passe incorrect");
		} finally {
			session.close();
		}
		

	}

	// retire le token de la BDD
	public static JSONObject disconnect(String token) {
		Session s = SessionUtil.getSession();
		try {
		if (!TokenUtils.isTokenValid(s, token))
			return ServicesTools.serviceRefused("Vous êtes déjà déconnecté");
		Transaction t =s.beginTransaction();
		SessionSaver saver = (SessionSaver) s.load(SessionSaver.class, token);
		s.delete(saver);
		t.commit();
		return ServicesTools.serviceAccepted();
		} finally {
			s.close();
		}
	}
	// Fait passer le boolean "activated à false, supprime les post-its de la cérémonie courante si il y en a 
	public static JSONObject deleteOwnAccount(String token, String password) {
		Session session = SessionUtil.getSession();
		try {
		//vérifie le token
		if (!TokenUtils.isTokenValid(session, token))
			return ServicesTools.serviceRefused("Utilisateur non connecté, veuillez vous reconnecter");
		//vérifie le mot de passe
		if (!TokenUtils.isPassWordValidFromToken(session, token, password))
			return ServicesTools.serviceRefused("Mot de passe fourni incorrect. Impossible de supprimer votre compte.");
		//supprimer de la BDD
		session.beginTransaction();
		// Si u est mdc, cérifie qu'il en reste au moins un
		SessionSaver s = ((SessionSaver) session.load(SessionSaver.class, token));
		
		//System.out.println((Long) session.createQuery("SELECT count(*) FROM Utilisateur u WHERE u.master is true and u.activated is true").uniqueResult());
		
		//désactiver le compte
		s.getUser().setActivated(false);
		//supprimer les post-its courants
		PostItGestion.deleteUserPostits(s.getUser(),session);
		session.update(s.getUser());
		session.delete(s);
		
		if (s.getUser().isMaster()) {
			Long i = (Long) session.createQuery("SELECT count(*) FROM Utilisateur u WHERE u.master is true and u.activated is true").uniqueResult();
			if (i == 0) {
				session.getTransaction().rollback();
				return ServicesTools.serviceRefused("Impossible de supprimer son compte lorqu'on est le seul maître de cérémonie.");
			}
		}
		else {
			session.getTransaction().commit();
		} 
		JSONObject ret = ServicesTools.serviceAccepted();
		ret.put("mailsent", MailSender.sendMailDesactivationUser(s.getUser().getMail(), s.getUser().getNickname()));
		return ret;
		} finally {
			session.close();
		}
	}
	// Fait passer le boolean "activated à false, supprime les post-its de la cérémonie courante si il y en a pour un utilisitaeur identifié par son id
	public static JSONObject deleteUserAccount(String token, int idUser) {
		Session session = SessionUtil.getSession();
		try {
		//vérifie le token du MDC
		if (!TokenUtils.isTokenValid(session, token))
			return ServicesTools.serviceRefused("Token obsolète ou incorrect, veuillez vous reconnecter.");
		if (!TokenUtils.isMDCFromToken(session, token))
			return ServicesTools.serviceRefused("Vous n'avez pas les privilèges maitre de cérémonie. Impossible d'ajouter un utilisateur.");
		//vérifie que ce n'est pas notre compte qu'on supprime...
		Utilisateur userMDC = ((SessionSaver) session.load(SessionSaver.class, token)).getUser();
		if (userMDC.getId() == idUser) {
			return ServicesTools.serviceRefused("Impossible d'utiliser ce service pour supprimer son propre compte.");
		}
		Utilisateur u = session.load(Utilisateur.class, idUser);
		if (!u.isActivated())
			return ServicesTools.serviceRefused("Impossible de supprimer cet utilisateur : compte déjà supprimé.");
		session.beginTransaction();
		//désactiver le compte
		u.setActivated(false);
		//supprimer les post-its courants
		PostItGestion.deleteUserPostits(u,session);
		session.createQuery("DELETE FROM SessionSaver WHERE user = :user").setParameter("user", u).executeUpdate();
		session.update(u);
		session.getTransaction().commit();
		JSONObject ret = ServicesTools.serviceAccepted();
		ret.put("mailsent", MailSender.sendMailDesactivationUser(u.getMail(), u.getNickname()));
		return ret;
		} finally {
			session.close();
		}
	}
	//Ajoute un utilisateur dans la base
	public static JSONObject addUser(String token, String mail, String user, String isMdc) {
		Session session = SessionUtil.getSession();
		try {
			// vérifier que le token est valide et que c'est un mdc
			if (!TokenUtils.isTokenValid(session, token)) {
				return ServicesTools.serviceRefused("Token obsolète ou incorrect, veuillez vous reconnecter");
			}
			if (!TokenUtils.isMDCFromToken(session, token)) {
				return ServicesTools.serviceRefused("Vous n'avez pas les privilèges maitre de cérémonie. Impossible d'ajouter un utilisateur");
			}
			//vérifier la longeur du mail(64 caractéres) et nickname(32 caractéres)
			if (mail.length() > 64 ) {
				return ServicesTools.serviceRefused("Le mail doit contenir moins de 64 caractères");
			}
			if (user.length() > 32) {
				return ServicesTools.serviceRefused("Le username doit contenir moins de 32 caractères");
			}
			// vérifier que le mail et le nickname sont libres
			Query query = session.createQuery("FROM Utilisateur as u WHERE u.mail = :mail");
			query.setParameter("mail", mail);
			Utilisateur u =(Utilisateur) query.uniqueResult();
			if (u==null) {
				//mail est inutilisé, on ajoute l'user
				session.beginTransaction();
				u = new Utilisateur();
				u.setMail(mail);
				u.setNickname(user);
				u.setActivated(true);
				u.setMaster(isMdc.equals("true"));
				String password = PasswordUtils.generatePassword();
				String salt = PasswordUtils.generateSalt();
				u.setPassword(PasswordUtils.getHash(password+salt));
				u.setSalt(salt);
				session.save(u);
				session.getTransaction().commit();
				// Insérer
				JSONObject res = ServicesTools.serviceAccepted();
				res.put("mailSent", MailSender.sendMailCreationUser(mail, user, password, isMdc.equals("true")));
				return res;
			} else {
				//un membre -> on regarde si il est activé
				if (u.isActivated())
					return ServicesTools.serviceRefused("Un utilisateur avec cette adresse mail existe déjà");
				session.beginTransaction();
				u.setActivated(true);
				u.setNickname(user);
				session.update(u);
				session.getTransaction().commit();
				JSONObject res = ServicesTools.serviceAccepted();
				res.put("mailSent", MailSender.sendMailReactivationUser(u.getMail(), u.getNickname()));
				return res;
			}
		} catch (ConstraintViolationException e) {
			return ServicesTools.serviceRefused("Un utilisateur avec ce nom existe déjà");//Les utilisateurs supprimés seront toujours dans la BD et ne pourront pas être réajoutés...
		} finally {
			session.close();
		}
	}
	//Permet à un admin de récupérer la liste de tous les utilisateurs
	public static JSONObject getUsers(String token) {
		Session session = SessionUtil.getSession();
		// vérifier que le token est valide et que c'est un mdc
		try {
			if (!TokenUtils.isTokenValid(session, token))
				return ServicesTools.serviceRefused("Token obsolète, veuillez vous connecter");
			if(!TokenUtils.isMDCFromToken(session, token))
				return ServicesTools.serviceRefused("Seul un maître de cérémonie peut accéder à la liste des utilisateurs");
			List<Utilisateur> result = session.createQuery("From Utilisateur WHERE activated is true").list();// On récupère tous les utilisateurs non détruits
			//Est-ce qu'on a une cérémonie en cours ?
			Query query = session.createQuery("FROM Ceremony as c WHERE (c.deadline > :date)");
			query.setParameter("date", new Date(System.currentTimeMillis()));
			Ceremony c = (Ceremony)query.uniqueResult();
			
			JSONArray arr = new JSONArray();
			for (Utilisateur u : result) {
				JSONObject o = new JSONObject();
				o.put("id", u.getId());
				o.put("nick", u.getNickname());
				o.put("mail", u.getMail());
				o.put("mdc", u.isMaster()?"true":"false");
				if (c != null) {
					List<Object> l =session.createQuery("FROM PostIt as p WHERE p.ceremony= :cerem AND p.user= :user").setParameter("cerem", c).setParameter("user", u).list();
					o.put("wrotepostit", l.isEmpty()?"false": "true");
				} else {
					o.put("wrotepostit","false");
				}
				arr.add(o);
			}
			JSONObject ret = ServicesTools.serviceAccepted();
			ret.put("users", arr);
			return ret;
		} finally {
			session.close();
		}
	}

	public static JSONObject changePassword(String token, String oldPassword, String newPassword) {
		Session session = SessionUtil.getSession();
		try {
			if (!TokenUtils.isTokenValid(session, token))
				return ServicesTools.serviceRefused("Token obsolète ou incorrect, veuillez vous reconnecter");
			if (!TokenUtils.isPassWordValidFromToken(session, token, oldPassword))
				return ServicesTools.serviceRefused("Mot de passe incorrect. Impossible de modifier le mot de passe.");
			if (newPassword.length() < 6) {
				return ServicesTools.serviceRefused("Le Mot de passe doit contenir au moins 6 caractères");
			}
			Transaction t = session.beginTransaction();
			Utilisateur u = ((SessionSaver) session.load(SessionSaver.class, token)).getUser();
			u.setPassword(PasswordUtils.getHash(newPassword+u.getSalt()));
			session.update(u);
			t.commit();
			return ServicesTools.serviceAccepted();
		} finally {
			session.close();
		}
	}

	// on ne peut pas changer son propre rôle
	public static Object changeRole(String token, int idUser, String newRole) {
		Session session = SessionUtil.getSession();
		try {
			if (!TokenUtils.isTokenValid(session, token))
				return ServicesTools.serviceRefused("Token obsolète ou incorrect, veuillez vous reconnecter");
			if (!TokenUtils.isMDCFromToken(session, token))
				return ServicesTools.serviceRefused("Seul un maître de cérémonie peut changer le rôle d'un utilisateur.");
			if (idUser == session.load(SessionSaver.class, token).getUser().getId())
				return ServicesTools.serviceRefused("On ne peut pas modifier son propre rôle.");
			session.beginTransaction();
			Utilisateur u = session.get(Utilisateur.class, idUser);
			if (u == null)
				return ServicesTools.serviceRefused("Impossible de trouver un utilisateur avec cet id");
			switch(newRole) {
				case "mdc" : 
					u.setMaster(true); break;
				case "user" : 
					u.setMaster(false); break;
				default: 
			}
			session.update(u);
			session.getTransaction().commit();
			return ServicesTools.serviceAccepted();
		} finally {
			session.close();
		}
	}

	public static Object resetPassword(String login) {
		Session session = SessionUtil.getSession();
		try {
			Transaction t = session.beginTransaction();
			Utilisateur u = (Utilisateur) session.createQuery("FROM Utilisateur as u WHERE u.nickname = :nick OR u.mail = :email").setParameter("nick", login).setParameter("email", login).uniqueResult();
			if (u == null) {
				return ServicesTools.serviceRefused("Aucun utilisateur trouvé avec ce login.");
			}
			if (!u.isActivated()) {
				return ServicesTools.serviceRefused("L'utilisateur a bien été trouvé mais le compte est désactivé.");
			}
			String newPassword = PasswordUtils.generatePassword();
			u.setPassword(PasswordUtils.getHash(newPassword+u.getSalt()));
			session.update(u);
			t.commit();
			JSONObject res = ServicesTools.serviceAccepted();
			res.put("mailSent", MailSender.sendMailPasswordReset(u.getMail(), newPassword));
			return res;
		} finally {
			session.close();
		}
	}
}
