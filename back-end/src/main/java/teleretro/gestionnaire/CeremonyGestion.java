package teleretro.gestionnaire;

import java.sql.Timestamp;
import java.util.Date;
import java.util.List;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

import org.hibernate.Session;
import org.hibernate.query.Query;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
//import org.json.simple.JSONArray;

import teleretro.beans.Ceremony;
import teleretro.beans.PostIt;
import teleretro.beans.SessionSaver;
import teleretro.gestionnaire.exceptions.AlreadyExistException;
import teleretro.gestionnaire.exceptions.CeremonyEndedException;
import teleretro.gestionnaire.exceptions.DidntStartException;
import teleretro.utils.MailSender;
import teleretro.utils.ServicesTools;
import teleretro.utils.SessionUtil;
import teleretro.utils.TokenUtils;


public class CeremonyGestion {

	//Ajoute une cérémonie dans la BDD
	public static JSONObject defineNew(String token, String name, String date) {
		synchronized(CeremonyGestion.class){
			Session session = SessionUtil.getSession();
			if(!TokenUtils.isTokenValid(session, token)) {
				return ServicesTools.serviceRefused("Utilisateur non connecté, veuillez vous reconnecter");
			}
			//vérifier que le token est valide et MDC
			if(!TokenUtils.isMDCFromToken(session, token)) {
				return ServicesTools.serviceRefused("token MDC invalide");
			}
			
			Timestamp deadline = Timestamp.valueOf(date);
			// vérifier que la date n'est pas antérieur à aujourd'hui.
			if(deadline.getTime() < new Date().getTime()) {
				return ServicesTools.serviceRefused("date non valide (antérieure)");
			}
			
			//vérifier la taille du nom (au moins 4 caractéres)
			if (name.length() < 4) {
				return ServicesTools.serviceRefused("Le nom de la cérémony doit contenir au moins 4 caractères");
			}
			
			//vérifier que le nom n'est pas déja utilisé
			Query query = session.createQuery("FROM Ceremony as c WHERE c.name = :name");
			query.setParameter("name", name);
			
			try {
				
				Ceremony c = (Ceremony)query.uniqueResult();
				
				if(c != null) {
					return ServicesTools.serviceRefused("Ce nom de cérémonie est déja utilisé");
				}
				
				//vérifier qu'il n'y a aucune cérémonie en post-cérémonie
				query = session.createQuery("FROM Ceremony as c WHERE c.ceremonyOver is false");
				
				List<Ceremony> ceremonies = query.list();
						
				if (ceremonies.size()>0) {
					return ServicesTools.serviceRefused("Il existe déja une ceremonie en cours.");
				}
				
				//notifier les utilisateurs de l'approche de la deadline(24h avant)
				Date d = deadline; 
				Date desiredDate = new Date (d.getTime() - 24 * 3600 * 1000); // -24heur
				Date now = new Date();
				long delay = desiredDate.getTime() - now.getTime();
				if (delay>0) {
					ScheduledExecutorService ses = Executors.newSingleThreadScheduledExecutor();
					ses.schedule(new Runnable(){
							  @Override
							  public void run() {
							     if (MailSender.sendMailDeadlineReminder())
							    	 System.out.println("notification envoyé");
							     else
							    	 System.out.println("erreur de l'envoi de rappel de deadline");
							  }
					}, delay, TimeUnit.MILLISECONDS);
				}
				//insertion de la cérémonie
				JSONObject res = ServicesTools.serviceAccepted();
				session.beginTransaction();
				
				session.save(new Ceremony(0, name, null, deadline, false));
				session.getTransaction().commit();
				res.put("mailsent", MailSender.sendMailCreationCeremony(name, deadline));
				return res;
			}
			finally {
				session.close();
			}
			
		}
	}
	
	public static JSONObject startCeremony(String token) {
		Session session = SessionUtil.getSession();
		if(!TokenUtils.isTokenValid(session, token)) {
			return ServicesTools.serviceRefused("Utilisateur non connecté, veuillez vous reconnecter");
		}
		//vérifier que le token est valide et MDC
		if(!TokenUtils.isMDCFromToken(session, token)) {
			return ServicesTools.serviceRefused("token MDC invalide");
		}
		
		//récupérer la post-cérémonie en cours.
		Query query = session.createQuery("FROM Ceremony as c WHERE c.ceremonyOver is false");
				
		try {
			
			Ceremony c = (Ceremony)query.uniqueResult();
			
			if (c == null) {
				return ServicesTools.serviceRefused("aucune post-ceremonie en cours.");
			}
			//vérifier que la deadline est bien passée
			if (c.getDeadline().after(new Date(System.currentTimeMillis())))
				return ServicesTools.serviceRefused("La deadline de la cérémonie n'est pas encore passée.");
			
			//update la start_date de la cérémonie en cours.
			JSONObject res = ServicesTools.serviceAccepted();
			session.beginTransaction();
			c.setStart_date(new Timestamp(System.currentTimeMillis()));
			session.update(c);
			session.getTransaction().commit();
			CeremonyLive.createCeremony(c);
			return res;
			
		} catch (AlreadyExistException e){
			return ServicesTools.serviceRefused("Une cérémonie est déjà en cours");
		}
		finally {
			session.close();
		}
	}

	//Renvoie la liste des cérémonies passées
	public static JSONObject consultHistory(String token) {
		Session session = SessionUtil.getSession();
		try {
		//vérifie le token
		if (!TokenUtils.isTokenValid(session, token))
			return ServicesTools.serviceRefused("Utilisateur non connecté, veuillez vous reconnecter");
		if (!TokenUtils.isMDCFromToken(session, token))
			return ServicesTools.serviceRefused("Seul un maître de cérémonie peut accéder à l'historique.");
		List<Ceremony> ceremonies = session.createQuery("From Ceremony").list();
		JSONArray cerems = new JSONArray();
		for (Ceremony c : ceremonies) {
			JSONObject temp = new JSONObject();
			temp.put("id", c.getId());
			temp.put("name", c.getName());
			temp.put("startDate",c.getStart_date());
			cerems.add(temp);
		}
		JSONObject ret = ServicesTools.serviceAccepted();
		ret.put("ceremonies", cerems);
		return ret;
		} finally {
			session.close();
		}
	}

	public static JSONObject participateCeremony(String token) {
		Session session = SessionUtil.getSession();
		try {
		//vérifie le token
		if (!TokenUtils.isTokenValid(session, token))
			return ServicesTools.serviceRefused("Utilisateur non connecté, veuillez vous reconnecter");
		//On s'insère dans la session
		SessionSaver s = ((SessionSaver) session.load(SessionSaver.class, token));
		
		if (CeremonyLive.getInstance().addParticipant(s.getUser()))
			return ServicesTools.serviceAccepted();
		return ServicesTools.serviceRefused("L'utilisateur participe déjà à la cérémonie ou la phase d'inscription est terminée");
		} catch (DidntStartException e) {
			return ServicesTools.serviceRefused("Pas de cérémonie en cours");
		} finally {
			session.close();
		}
	}

	public static JSONObject getParticipantsCeremony(String token, int count) {
		Session s = SessionUtil.getSession();
		try {
			//vérifie le token
			if (!TokenUtils.isTokenValid(s, token))
				return ServicesTools.serviceRefused("Utilisateur non connecté, veuillez vous reconnecter");
			JSONObject temp = ServicesTools.serviceAccepted();
			temp.put("participants", CeremonyLive.getInstance().getAllParticipants(count));
			return temp;
		} catch (DidntStartException e) {
			return ServicesTools.serviceRefused("Aucune cérémonie n'a démarré.");
		} catch (InterruptedException e) {
			return ServicesTools.serviceRefused("Erreur synchronisation java.t");
		} finally {
			s.close();
		}
	}

	public static JSONObject distribute(String token) {
		Session s = SessionUtil.getSession();
		try {
		//vérifie le token
		if (!TokenUtils.isMDCFromToken(s, token))
			return ServicesTools.serviceRefused("token MDC invalide");
		//On ferme l'inscription à la cérémonie
		//On déclenche la répartition des post-its
		if (CeremonyLive.getInstance().isRepartitionDone())
			return ServicesTools.serviceRefused("La répartition a déjà été effectuée");
		CeremonyLive.getInstance().repartition(s);
		//On déclenche le résultat de getPostItCerem
		//On renvoie c'est bon
		return ServicesTools.serviceAccepted();
		} catch (DidntStartException e) {
			return ServicesTools.serviceRefused("L'appel au service distribuer a été effectué alors qu'aucune cérémonie n'a été lancée.");
		} finally {
			s.close();
		}
	}

	public static JSONObject whoSTurn(String token, int tour) {
		Session session = SessionUtil.getSession();
		try {
			//vérifie le token
			if (!TokenUtils.isTokenValid(session, token))
				return ServicesTools.serviceRefused("Utilisateur non connecté, veuillez vous reconnecter");
			JSONObject jo = CeremonyLive.getInstance().nextParticipant(tour);
			SessionSaver s = session.load(SessionSaver.class, token);
			if (jo.get("de") != null) {
				if (s.getUser().getNickname().equals(((JSONObject)jo.get("de")).get("nickname"))) 
					jo.put("yourturn", true);
			}
			JSONObject ret = ServicesTools.serviceAccepted();
			ret.put("answer", jo);
			return ret;
		} catch (InterruptedException e) {
			return ServicesTools.serviceRefused("Erreur de concurrence java");
		} catch (DidntStartException e) {
			return ServicesTools.serviceRefused("Aucune cérémonie en cours");
		} finally {
			session.close();
		}
	}

	public static JSONObject turnIsOver(String token, int tour, boolean byMdc) {
		Session session = SessionUtil.getSession();
		try {
			//vérifie le token
			if (!TokenUtils.isTokenValid(session, token))
				return ServicesTools.serviceRefused("Utilisateur non connecté, veuillez vous reconnecter");
			if (byMdc) {
				if (!TokenUtils.isMDCFromToken(session, token))
					return ServicesTools.serviceRefused("token MDC invalide");
				if (CeremonyLive.getInstance().endTurnByMdc(tour))
					return ServicesTools.serviceAccepted();
				return ServicesTools.serviceRefused("Vous avez demandé de finir un tour qui n'est pas en cours.");
			}
			//On récupère le nom à partir du token
			if (CeremonyLive.getInstance().endTurn(tour, session.load(SessionSaver.class, token).getUser().getNickname()))
				return ServicesTools.serviceAccepted();
			return ServicesTools.serviceRefused("Ce n'est pas à vous de finir votre tour.");
			
		} catch (DidntStartException e) {
			return ServicesTools.serviceRefused("Impossible d'appeler le service, pas de cérémonie en cours");
		} catch (CeremonyEndedException e) {
			//On a atteint la fin de la cérémonie, on la clotûre automatiquement
			session.beginTransaction();
			Ceremony c;
			try {
				c = CeremonyLive.getInstance().getCeremony();
				c.setIsOver(true);
				session.update(c);
				session.getTransaction().commit();
				CeremonyLive.closeInstance();
				return ServicesTools.serviceAccepted();
			} catch (DidntStartException e1) {
				return ServicesTools.serviceRefused("Erreur lors de la fermeture de la cérémonie");
			}
		} finally {
			session.close();
		}
	}

	public static JSONObject close(String token) {
		Session session = SessionUtil.getSession();
		try {
			//vérifie le token
			if (!TokenUtils.isTokenValid(session, token))
				return ServicesTools.serviceRefused("Utilisateur non connecté, veuillez vous reconnecter");
			//vérifie qu'on est bien un mdc
			if (!TokenUtils.isMDCFromToken(SessionUtil.getSession(), token))
				return ServicesTools.serviceRefused("token MDC invalide");
			CeremonyLive.closeInstance();
		} catch (DidntStartException e) {
			return ServicesTools.serviceRefused("Aucune session en cours");
		} finally {
			session.close();
		}
		return ServicesTools.serviceAccepted();
	}

	public static JSONObject getCurrentCeremony(String token) {
			Session session = SessionUtil.getSession();
			try {
				//vérifie le token
				if (!TokenUtils.isTokenValid(session, token))
					return ServicesTools.serviceRefused("Utilisateur non connecté, veuillez vous reconnecter");
				Ceremony ceremony = (Ceremony) session.createQuery("From Ceremony as c WHERE c.ceremonyOver is false").uniqueResult();
				if (ceremony == null) {
					JSONObject ret = ServicesTools.serviceAccepted();
					ret.put("idCeremony", -1);
					return ret;
				}
				JSONObject ret = ServicesTools.serviceAccepted();
				ret.put("idCeremony", ceremony.getId());
				ret.put("nameCeremony", ceremony.getName());
				ret.put("deadlineCeremony",ceremony.getDeadline());
				ret.put("startdate",ceremony.getStart_date());
				return ret;
			} finally {
				session.close();
			}
	}

	public static JSONObject moveDeadlineCurrentCeremony(String token, String date) {
		Session session = SessionUtil.getSession();
		try {
			//vérifie le token
			if (!TokenUtils.isTokenValid(session, token))
				return ServicesTools.serviceRefused("Utilisateur non connecté, veuillez vous reconnecter");
			//vérifie qu'on est bien un mdc
			if (!TokenUtils.isMDCFromToken(SessionUtil.getSession(), token))
				return ServicesTools.serviceRefused("token MDC invalide");
			//Est-ce qu'il y a une cérémonie qu'on peut modifier ? (La cérémonie ne doit pas avoir commencé)
			Ceremony ceremony = (Ceremony) session.createQuery("From Ceremony as c WHERE c.start_date is null").uniqueResult();
			if (ceremony == null)
				return ServicesTools.serviceRefused("Pas de cérémonie en état de post-cérémonie.");
			Timestamp deadline = Timestamp.valueOf(date);//IllegalArgumentException si mauvais format de date
			// vérifier que la date n'est pas antérieur à aujourd'hui.
			if(deadline.before(new Date(System.currentTimeMillis()))) {
				return ServicesTools.serviceRefused("date non valide (antérieure à aujourd'hui)");
			}
			//mettre à jour
			session.beginTransaction();
			ceremony.setDeadline(deadline);
			session.saveOrUpdate(ceremony);
			session.getTransaction().commit();
			return ServicesTools.serviceAccepted();
		} catch (IllegalArgumentException e) {
			return ServicesTools.serviceRefused("Mauvais format de date.");
		}
		finally {
			session.close();
		}
	}

}
