package teleretro.gestionnaire;

import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;

import org.hibernate.Session;
import org.hibernate.query.Query;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

import teleretro.beans.Ceremony;
import teleretro.beans.PostIt;
import teleretro.beans.Utilisateur;
import teleretro.gestionnaire.exceptions.AlreadyExistException;
import teleretro.gestionnaire.exceptions.CeremonyEndedException;
import teleretro.gestionnaire.exceptions.DidntStartException;
import teleretro.gestionnaire.exceptions.UtilisateurIsNotParticipatingException;

public class CeremonyLive {
	private enum State{
		INSCRIPTION,
		DISCUSSION,
		FINIE
	}
	
	private static CeremonyLive cerem;
	
	private State state;
	private Ceremony ceremony;
	
	private int compteurTour;
	private boolean repartitionDone;

	private Object lockInscription;
	private Object lockPostItRecup;
	private Object lockTour;
	
	private List<JSONObject> users;
	private HashMap<String, JSONArray> repartPostIt;
	
	private CeremonyLive(Ceremony c) {
		this.state = State.INSCRIPTION;
		this.ceremony = c;
		
		this.compteurTour = 0;
		this.repartitionDone = false;
		
		this.lockInscription = new Object();
		this.lockPostItRecup = new Object();
		this.lockTour = new Object();

		this.users = new LinkedList<>();
		this.repartPostIt = new HashMap<>();
	}
	
	public static synchronized CeremonyLive createCeremony(Ceremony c) throws AlreadyExistException {
		if (cerem != null)
			throw new AlreadyExistException();
		cerem = new CeremonyLive(c);
		return cerem;
	}
	
	public static CeremonyLive getInstance() throws DidntStartException {
		if(cerem == null)
			throw new DidntStartException();
		return cerem;
	}
	
	public static void closeInstance() throws DidntStartException {
		if (cerem == null)
			throw new DidntStartException();
		cerem.state=State.FINIE;
		cerem=null;
	}
	
	public Ceremony getCeremony() {
		return this.ceremony;
	}
	public boolean isRepartitionDone() {
		return this.repartitionDone;
	}
	
	
	public boolean addParticipant(Utilisateur u) {
		synchronized(lockInscription) {
			//On ne peut participer que si la cérémonie est à l'état Inscription
			if (state != State.INSCRIPTION)
				return false;
			//Si l'utilisateur est déjà parmi dans les participants on renvoie false sinon, on l'ajoute dans la liste.
			for (JSONObject jo : users) {
				if (u.getNickname().equals(jo.get("nickname")))
					return false;
			}
			JSONObject temp = new JSONObject();
			temp.put("nickname", u.getNickname());
			users.add(temp);
			lockInscription.notifyAll();
			return true;
		}
	}
	
	public JSONObject nextParticipant(int tour) throws InterruptedException {
		synchronized (lockTour) {
			while(tour==compteurTour) {
				lockTour.wait();
			}
			if(compteurTour==users.size()) {
				//Si tous les participants ont eu leur tour
				JSONObject ret = new JSONObject();
				ret.put("tour", compteurTour);
				ret.put("fini", true);
				return ret;
			}
			JSONObject ret = new JSONObject();
			ret.put("tour", compteurTour);
			ret.put("de", users.get(compteurTour));
			return ret;
		}
	}
	
	public boolean endTurn(int tour, String nickname) throws CeremonyEndedException{
		synchronized (lockTour) {
			//On vérifie que la demande de passer le tour correspond au bon tour et est faite par le bon utilisateur
			if (tour==compteurTour && nickname.equals(users.get(compteurTour).get("nickname"))) {
				compteurTour++;
				lockTour.notifyAll();
				//Est-ce qu'on vient de finir la cérémonie ?
				if (compteurTour==users.size())
					throw new CeremonyEndedException();
				return true;
			}
			return false;
		}
	}
	
	public boolean endTurnByMdc(int tour) throws CeremonyEndedException{
		synchronized (lockTour) {
			//On vérifie que la demande de passer le tour correspond au bon tour
			if (tour==compteurTour) {
				compteurTour++;
				lockTour.notifyAll();
				//Est-ce qu'on vient de finir la cérémonie ?
				if (compteurTour==users.size())
					throw new CeremonyEndedException();
				return true;
			}
			return false;
		}
	}
	
	public JSONArray getAllRemainingParticiapant() {
		JSONArray temp = new JSONArray();
		for (JSONObject jo : users)
			temp.add(jo);
		return temp;
	}
	
	@SuppressWarnings("unchecked")
	public JSONArray getAllParticipants(int prevCount) throws InterruptedException {
		if (state == State.DISCUSSION) {
			JSONArray temp = new JSONArray();
			for (String jo : repartPostIt.keySet())
				temp.add(jo);
			return temp;
		}
		//si INSCRIPTION et prevCount==participants.size(), on attend. Sinon, on renvoie les participants
		synchronized (lockInscription) {
			while (state == State.INSCRIPTION && prevCount == users.size()) {
				lockInscription.wait();
			}
		}
		JSONArray temp = new JSONArray();
		for (JSONObject jo : users)
			temp.add(jo);
		return temp;
	}

	@SuppressWarnings("unchecked")
	public void repartition(Session s) {
		synchronized(lockInscription) {
			if (!repartitionDone) {
				//On récupère les post-its qui en font partie
				List<PostIt> result = s.createQuery("From PostIt WHERE ceremony= :ceremony").setParameter("ceremony", this.ceremony).list();
				//On récupère tous les auteurs de post-its pour cette cérémonie
				Set<String> auteurs = new HashSet<>();
				for (PostIt p : result)
					auteurs.add(p.getUser().getNickname());
				//On répartit les auteurs entre les gens présents
				List<String>[] repart= new List[users.size()];
				for(int i = 0; i< repart.length; i++) {
					repart[i] = new LinkedList<String>();
				}
				int nextInsert = 0;
				for(String auteur: auteurs) {
					if(!auteur.equals(users.get(nextInsert).get("nickname"))) {
					//Si on peut l'insérer dedans on le fait
						repart[nextInsert].add(auteur);
					} else {
					//Sinon on échange avec le premier
						if (repart[0].isEmpty()) {
							//cas particulier tout premier insert -> on le file au dernier
							repart[repart.length-1].add(auteur);
							nextInsert--;
						} else {
							String tmp = repart[0].remove(0);
							repart[0].add(auteur);
							repart[nextInsert].add(tmp);
						}
						
					}
					nextInsert = (nextInsert+1)%users.size();
				}
				//On charge les post-its que chacun devra lire dans la Map
				//Le joli O(n³)
				for(int i = 0; i< repart.length; i++) {
					JSONArray ja = new JSONArray();
					//C'est vraiment pas opti là...
					for(String auteur: repart[i]) {
						for(PostIt p : result) {
							if (p.getUser().getNickname().contentEquals(auteur)) {//on l'ajoute
								JSONObject jo = new JSONObject();
								jo.put("content", p.getContent());
								jo.put("author", p.getUser().getNickname());
								jo.put("positif", p.isType());
								jo.put("present", auteurs.contains(p.getUser().getNickname()));
								ja.add(jo);
							}
						}
						
					}
					this.repartPostIt.put((String) users.get(i).get("nickname"), ja);
				}
				
				//On tire un ordre de passage aléatoire
				Collections.shuffle(users);	
				//On rentre dans l'état où on peut récupérer ses post-its
				this.repartitionDone=true;
				this.state = State.DISCUSSION;
				lockInscription.notifyAll();
			}
			synchronized(lockPostItRecup) {
				lockPostItRecup.notifyAll();
			}
		}
	}
	
	public JSONArray getMyPostIts(String nick) throws InterruptedException, UtilisateurIsNotParticipatingException {
		//vérifier qu'on est bien parmi les inscrits
		boolean isInscrit = false;
		for (JSONObject jo : users) {
			if (nick.equals(jo.get("nickname"))) {
				isInscrit = true;
				break;
			}
		}
		if (!isInscrit)
			throw new UtilisateurIsNotParticipatingException();
		//attendre qu'on puisse récupérer ses post-its
		synchronized (lockPostItRecup) {
			while (this.state == State.INSCRIPTION) {
				lockPostItRecup.wait();
			}
		}
		return repartPostIt.get(nick);
	}
	
	
}
