package teleretro;

import java.util.HashMap;
import java.util.Map;

import org.eclipse.jetty.server.Server;
import org.eclipse.jetty.servlet.ServletContextHandler;
import org.eclipse.jetty.servlet.ServletHolder;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.cfg.Configuration;

import teleretro.beans.Utilisateur;
import teleretro.services.UtilisateurServices;
import teleretro.utils.SessionUtil;

public class App {
    public static void main(String[] args) throws Exception {
        ServletContextHandler context = new ServletContextHandler(ServletContextHandler.SESSIONS);
        context.setContextPath("/");
        
        Server jettyServer = new Server(8080);
        jettyServer.setHandler(context);
        //context.setServer(jettyServer);

        ServletHolder jerseyServlet = context.addServlet(
             org.glassfish.jersey.servlet.ServletContainer.class, "/*");
        jerseyServlet.setInitOrder(0);
        
        // Tells the Jersey Servlet which REST service/class to load.
        jerseyServlet.setInitParameter(
           "jersey.config.server.provider.packages",
           "teleretro.services");
        
        try {
            jettyServer.start();
            jettyServer.join();
        } finally {
        	SessionUtil.closeSessionFactory();
            jettyServer.destroy();
        }
    }
}