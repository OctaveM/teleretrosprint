package teleretro.beans;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;



@Entity
@Table(name = "user")
public class Utilisateur {
	
	@Id
	@Column(name ="user_id")
	private int id;
	@Column(name ="user_nickname")
	private String nickname;
	@Column(name ="user_mail")
	private String mail;
	@Column(name ="user_password")
	private String password;
	@Column(name ="user_salt")
	private String salt;
	@Column(name ="user_ceremony_master")
	private boolean master;
	@Column(name ="user_activated")
	private boolean activated;

	public Utilisateur() {}
	
	public Utilisateur(int id, String nickname, String mail, String password, String salt, boolean master, boolean activated) {
		super();
		this.id = id;
		this.nickname = nickname;
		this.mail = mail;
		this.password = password;
		this.salt =salt;
		this.master = master;
		this.activated = activated;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getNickname() {
		return nickname;
	}

	public void setNickname(String nickname) {
		this.nickname = nickname;
	}
	
	public String getMail() {
		return mail;
	}

	public void setMail(String mail) {
		this.mail = mail;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}
	
	public String getSalt() {
		return salt;
	}

	public void setSalt(String salt) {
		this.salt = salt;
	}

	public boolean isMaster() {
		return master;
	}

	public void setMaster(boolean master) {
		this.master = master;
	}

	public boolean isActivated() {
		return activated;
	}

	public void setActivated(boolean activated) {
		this.activated = activated;
	}
	
}
