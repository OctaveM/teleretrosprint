package teleretro.beans;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "post_it")
public class PostIt {
	@Id
	@Column(name = "post_it_id")
	private int id;
	@ManyToOne
    @JoinColumn(name = "user_id")
	private Utilisateur user;
	@ManyToOne
	@JoinColumn(name = "ceremony_id")
	private Ceremony ceremony;
	@Column(name = "type")
	private boolean type;
	@Column(name = "content")
	private String content;
	
	public PostIt() {
		
	}

	public PostIt(int id, Utilisateur user, Ceremony ceremony, boolean type, String content) {
		super();
		this.id = id;
		this.user = user;
		this.ceremony = ceremony;
		this.type = type;
		this.content = content;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public Utilisateur getUser() {
		return user;
	}

	public void setUser(Utilisateur user) {
		this.user = user;
	}

	public Ceremony getCeremony() {
		return ceremony;
	}

	public void setCeremony(Ceremony ceremony) {
		this.ceremony = ceremony;
	}

	public boolean isType() {
		return type;
	}

	public void setType(boolean type) {
		this.type = type;
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}
	
}

