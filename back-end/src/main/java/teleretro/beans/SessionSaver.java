package teleretro.beans;

import java.sql.Date;
import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "session_saver")
public class SessionSaver {

	@Id
	@Column(name = "session_key")
	private String token;
	
	@ManyToOne
    @JoinColumn(name = "user_id")
	private Utilisateur user;
	@Column(name = "session_date")
	private Timestamp session_date;
	
	public SessionSaver() {}
	
	public SessionSaver(String token, Utilisateur user, Timestamp session_date) {
		this.token=token;
		this.user=user;
		this.session_date=session_date;
	}

	public String getToken() {
		return token;
	}

	public void setToken(String token) {
		this.token = token;
	}

	public Utilisateur getUser() {
		return user;
	}

	public void setUser(Utilisateur user) {
		this.user = user;
	}

	public Timestamp getSession_date() {
		return session_date;
	}

	public void setSession_date(Timestamp session_date) {
		this.session_date = session_date;
	}
	
}
