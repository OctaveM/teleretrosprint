package teleretro.beans;

import java.sql.Timestamp;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "ceremony")
public class Ceremony {

	@Id
	@Column(name = "ceremony_id")
	private int id;
	@Column(name = "ceremony_name")
	private String name;
	@Column(name = "ceremony_start_date ")
	private Timestamp start_date;
	@Column(name = "ceremony_deadline")
	private Timestamp deadline;
	@Column(name = "ceremony_is_over ")
	private boolean ceremonyOver;
	
	public Ceremony() {

	}

	public Ceremony(int id, String name, Timestamp start_date, Timestamp deadline, boolean isOver) {
		super();
		this.id = id;
		this.name = name;
		this.start_date = start_date;
		this.deadline = deadline;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Date getStart_date() {
		return start_date;
	}

	public void setStart_date(Timestamp start_date) {
		this.start_date = start_date;
	}

	public Date getDeadline() {
		return deadline;
	}

	public void setDeadline(Timestamp deadline) {
		this.deadline = deadline;
	}
	
	public boolean getIsOver() {
		return this.ceremonyOver;
	}

	public void setIsOver(boolean isOver) {
		this.ceremonyOver = isOver;
	}
	
}
