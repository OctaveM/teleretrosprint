package teleretro.test;

import java.sql.Timestamp;
import java.util.Date;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

public class testScheduler {

	public static void main(String[] args) {
		
		Timestamp deadline = Timestamp.valueOf("2021-01-25 17:27:03.0" );
		Date d = deadline; 
		//notifier les utilisateurs de l'approche de la deadline(24h avant)
		Date desiredDate = new Date (d.getTime() - 24 * 3600 * 1000); // -24heur
		Date now = new Date();
		long delay = desiredDate.getTime() - now.getTime();
		System.out.println(desiredDate);
		ScheduledExecutorService ses = Executors.newSingleThreadScheduledExecutor();
		ses.schedule(new Runnable(){
				  @Override
				  public void run() {
				     System.out.println("notification envoyé...");
				  }
		}, delay, TimeUnit.MILLISECONDS);
		
		// vérifier que la deadline n'est pas antérieur à aujourd'hui.
		/*if(deadline.getTime() < new Date().getTime()) {
			System.out.println("antérieur...");
		}*/
	}

}
