package teleretro.test;

import java.net.URI;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.Form;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriBuilder;

import org.glassfish.jersey.client.ClientConfig;

public class TestPostItServices {
	
	public static void main(String[] args) {
        ClientConfig config = new ClientConfig();

        Client client = ClientBuilder.newClient(config);
        
        WebTarget target = client.target(getBaseURI());
        
        System.out.println(testInsert(target,"9fe1311b-7675-4915-97f2-3e675a1e19c6","positif :)","negatif"));
    }

	private static String testInsert(WebTarget target, String token, String positif, String negatif ) {
		Form form = new Form();
		form.param("token", token);
		form.param("positif", positif);
		form.param("negatif", negatif);
        return target.path("postitresource").path("insert").request().post(Entity.entity(form, MediaType.APPLICATION_JSON), String.class);
	}
	
	/*@Test
	public void addUserTest() {
	    User user = new User();
	    user.setEmail("user2@mail.com");
	    user.setName("Jane Doe");
	    user.getUserRoles().getRoles().add("supertester");
	    Entity<User> userEntity = Entity.entity(user, MediaType.APPLICATION_XML_TYPE);
	    target("users/add").request().post(userEntity); //Here we send POST request
	    Response response = target("users/find").queryParam("email", "user2@mail.com").request().get(); //Here we send GET request for retrieving results
	    Assert.assertEquals("user2@mail.com", response.readEntity(User.class).getEmail());

	}*/
	
    private static URI getBaseURI() {
        return UriBuilder.fromUri("http://localhost:8080").build();
    }
}
