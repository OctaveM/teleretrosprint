package teleretro.services;

import javax.validation.constraints.NotNull;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.container.AsyncResponse;
import javax.ws.rs.container.Suspended;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import teleretro.gestionnaire.PostItGestion;

/**
 * Root resource (exposed at "postitresource" path)
 */
@Path("/postitresource")
public class PostItServices {

	/**
	 * Permet à un utilisateur d'ajouter des post-its pour la cérémonie en cours tant que la deadline de celle-ci n'est pas passée
	 * @param token		le token de l'utilisateur
	 * @param positif	le contenu du post-it positif
	 * @param negatif	le contenu du post-it négatif
	 * @return status OK si on a pu insérer les post-its
	 */
    @POST
    @Path("/insert")
    @Produces(MediaType.APPLICATION_JSON)
    public Response insertPostIt(@NotNull @QueryParam("token") String token, @NotNull @QueryParam("positif") String positif, @NotNull @QueryParam("negatif") String negatif) {
        return Response.ok()
                .entity(PostItGestion.insert(token, positif, negatif))
                .build();
    }	
	//http://localhost:8080/postitresource/insert?token=3c8f3cfe-a9ce-43e3-91fd-988b97ffe04a&positif=caml_est_le_meilleur_language_:) &negatif= vraiment_:) 

    /**
     * Permet à un utilisateur de modifier ses post-its
     * @param token		le token de l'utilisateur
     * @param positif	le contenu du post-it positif
     * @param negatif	le contenu du post-it négatif
     * @return	status OK si on a pu modifier le contenu des post-its
     */
    @POST
    @Path("/update")
    @Produces(MediaType.APPLICATION_JSON)
    public Response updatePostIt(@NotNull @QueryParam("token") String token, @NotNull @QueryParam("positif") String positif, @NotNull @QueryParam("negatif") String negatif) {
		return Response.ok()
                .entity(PostItGestion.update(token, positif, negatif))
                .build();
    }
	//http://localhost:8080/postitresource/update?token=078ec226-8b13-4875-8a94-c5f672db90eb&positif=mise à jour positif &negatif= mise à jour negatif 

    /**
     * Permet à un utilisateur de récupérer les post-its qu'il a écrit pour la cérémonie courante
     * @param token le toke nde l'utilisateur
     * @return 	status 	OK si on a pu récupérer les post-its
     * 			positif	le contenu du post-it positif
     * 			negatif	le contenu du post-it negatif
     */
    @GET
    @Path("/getOwnCurrent")
    @Produces(MediaType.APPLICATION_JSON)
    public Response getOwnPostIt(@NotNull @QueryParam("token") String token) {
        return Response.ok()
                .entity(PostItGestion.getOwnCurrent(token))
                .build();
    }
	//http://localhost:8080/postitresource/getOwnCurrent?token=3c8f3cfe-a9ce-43e3-91fd-988b97ffe04a 
    
    /**
     * Permet à un utilisateur de récupérer ses post-its pour la cérémonie.
     * L'utilisateur doit avoir appelé le service participate durant la phase d'Inscription de la cérémonie. Il recevra ses post-its lorsque la cérémonie sera en phase de Discussion
     * @param token	le token de l'utilisateur
     * @return 	status 	OK si on a pu récupérer les post-its
     * 			postits	la liste des post-its qu'il devra lire (contenant 	"content" : le contenu du post-it,
     * 																		"author" : l'auteur du post-it,
     * 																		"positif" : booléen indiquant si le post-it est positif ou négatif,
     *      																"present" : booléen indiquant si l'auteur est présent)
     */
    @GET
    @Path("/getpostitcerem")
    @Produces(MediaType.APPLICATION_JSON)
    public void getPostItCerem(@NotNull @QueryParam("token") final String token, @Suspended final AsyncResponse asyncResponse) {
    	new Thread(new Runnable() {
    		@Override
    		public void run() {
    			asyncResponse.resume(Response.ok()
    	                .entity(PostItGestion.getPostItCerem(token))
    	                .build());
    		}
    	}).start();
    }
    
    /**
     * Permet de récupérer les post-its qui ont été écrits pour une cérémonie
     * @param token		le token d'un MDC
     * @param idcerem	l'id de la cérémonie dont on veut récupérer les post-its
     * @return	status OK si on a pu récupérer les post-its
     * 			postits	la liste des post-its de la cérémonie (contenant 	"content" : le contenu du post-it,
     * 																		"author" : l'auteur du post-it,
     * 																		"positif" : booléen indiquant si le post-it est positif ou négatif,)
     */
    @GET
    @Path("/getpostitfromcerem")
    @Produces(MediaType.APPLICATION_JSON)
    public Response getPostItFromCerem(@NotNull @QueryParam("token") String token, @NotNull @QueryParam("idcerem") Integer idcerem) {
        return Response.ok()
                .entity(PostItGestion.getPostItFromCerem(token, idcerem))
                .build();
    }

}
