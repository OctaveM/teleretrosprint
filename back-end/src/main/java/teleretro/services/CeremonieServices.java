package teleretro.services;

import javax.validation.constraints.NotNull;
import javax.ws.rs.DefaultValue;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.container.AsyncResponse;
import javax.ws.rs.container.Suspended;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import teleretro.gestionnaire.CeremonyGestion;
import teleretro.gestionnaire.PostItGestion;
import teleretro.utils.ServicesTools;


/**
 * Root resource (exposed at "ceremonyresource" path)
 */
@Path("/ceremonyresource")
public class CeremonieServices {

		/**
		 * Service pour créer une cérémonie en état de post-cérémonie. A condition qu'aucune cérémonie soit en post-cérémonie.
		 * @param token	Le token d'un MDC
		 * @param name	Le nom de la future cérémonie
		 * @param date	La deadline de remplissage de post-its pour la cérémonie
		 * @return 	status 		OK si la cérémonie a bien été créée.
		 * 			mailsent	booléen indiquant si on a bien envoyé un mail à tous les utilisateurs actifs pour leur indiquer qu'on venait de créer une cérémonie
		 */
	    @GET//POST
	    @Path("/definenewceremony")
	    @Produces(MediaType.APPLICATION_JSON)
	    public Response defineNewCeremony(@NotNull @QueryParam("token") String token, @NotNull @QueryParam("name") String name, @NotNull @QueryParam("date") String date) {
	        return Response.ok()
	                .entity(CeremonyGestion.defineNew(token, name, date))
	                .build();
	        
	    }
		//http://localhost:8080/ceremonyresource/definenewceremony?token=cfd2cc4b-9879-45fa-be36-18c8fc21ac59&name=TheCeremony&date=2021-02-12 08:00:00.0
		//format date : "2021-01-12 01:02:03.0" 
	    
	    
		//Pour lancer la cérémonie : on est dans la phase où les utilisateurs viennent s'inscrire pour participer
	    /**
		 * Service pour lancer une cérémonie en état d'inscription. A condition qu'il existe une cérémonie en post-cérémonie.
		 * @param token	Le token d'un MDC
		 * @return status OK si la cérémonie a bien été passée en phase Inscription.
		 */
	    @GET
	    @Path("/start")
	    @Produces(MediaType.APPLICATION_JSON)
	    public Response start(@NotNull @QueryParam("token") String token) {
	        return Response.ok()
	                .entity(CeremonyGestion.startCeremony(token))
	                .build();
	    }
		//http://localhost:8080/ceremonyresource/start?token=cfd2cc4b-9879-45fa-be36-18c8fc21ac59
		
	    /**
		 * Service pour consulter la liste des cérémonies.
		 * @param token	Le token d'un MDC
		 * @return 	status 		OK si on est autorisé à accéder à la liste.
		 * 			Ceremonies	Liste des cérémonies (contenant "id", "name" et "startDate")
		 */
	    @GET
	    @Path("/consulthistory")
	    @Produces(MediaType.APPLICATION_JSON)
	    public Response consultHistory(@NotNull @QueryParam("token") String token) {
	        return Response.ok()
	                .entity(CeremonyGestion.consultHistory(token))
	                .build();
	    }
	    
	    /**
		 * Service pour récupérer les informations de la cérémonie en cours
		 * @param token	Le token d'un utilisateur
		 * @return 	status 		OK
		 * 			idCeremony	-1 si pas de cérémonie en cours ou l'id de la cérémonie
		 * 			nameceremony	si une cérémonie est en cours, son nom
		 * 			deadlineCeremony	si une cérémonie est en cours, sa deadline pour remplir des post-its
		 * 			startdate	si une cérémonie est en cours, sa date de départ (vaut null si la cérémonie est toujours en post-cérémonie)
		 */
	    @GET
	    @Path("/getcurrentcerem")
	    @Produces(MediaType.APPLICATION_JSON)
	    public Response getCurrentCeremony(@NotNull @QueryParam("token") String token) {
	        return Response.ok()
	                .entity(CeremonyGestion.getCurrentCeremony(token))
	                .build();
	    }
	    
	    /**
		 * Service pour modifier la deadline de la cérémonie en cours
		 * @param token	Le token d'un MDC
		 * @param date	La nouvelle date pour la deadline.
		 * @return 	status 		OK si la deadline fournie est bien dans le futur et qu'on a pu modifier la date de la deadline
		 */
	    @GET
	    @Path("/movedeadlinecurrentcerem")
	    @Produces(MediaType.APPLICATION_JSON)
	    public Response moveDeadlineCurrentCeremony(@NotNull @QueryParam("token") String token, @NotNull @QueryParam("date") String date) {
	        return Response.ok()
	                .entity(CeremonyGestion.moveDeadlineCurrentCeremony(token, date))
	                .build();
	    }
		
	    /**
		 * Service pour participer à la cérémonie en phase d'inscription (permet de recevoir des post-its à lire et d'avoir un tour de parole)
		 * @param token	Le token d'un utilisateur
		 * @return 	status 		OK si on a pu ajouter l'utilisateur aux participants de la cérémonie
		 */
	    @GET
	    @Path("/participate")
	    @Produces(MediaType.APPLICATION_JSON)
	    public Response participate(@NotNull @QueryParam("token") String token) {
	        return Response.ok()
	                .entity(CeremonyGestion.participateCeremony(token))
	                .build();
	    }
	    
	    /**
		 * Service pour obtenir la liste de tous les participants à la cérémonie
		 * @param token	Le token d'un utilisateur
		 * @param count	(Optionnel) Le nombre de participants que la personne appelant le service connaît. (Permet de ne renvoyer la liste des utilisateurs que si des personnes y ont été ajouté par rapport à la dernière requête)
		 * @return 	status 		OK si on a pu récupérer la liste des participants
		 * 			participants la liste des participants (Contenant "nickname" : le nom du participant)
		 */
	    @GET
	    @Path("/getparticipants")
	    @Produces(MediaType.APPLICATION_JSON)
	    public void getParticipants(@NotNull @QueryParam("token") final String token, @DefaultValue("0") @QueryParam("count") final Integer count, @Suspended final AsyncResponse asyncResponse) {
	    	new Thread(new Runnable() {
	    		@Override
	    		public void run() {
	    			asyncResponse.resume(Response.ok()
	    	                .entity(CeremonyGestion.getParticipantsCeremony(token, count))
	    	                .build());
	    		}
	    	}).start();
	    }
	    
	    /**
		 * Service pour faire passer la cérémonie de la phase d'inscription à la phase de discussion (et déclenche la distribution des post-its)
		 * @param token	Le token d'un MDC
		 * @return 	status 		OK si on a pu distribuer les post-its et changer la cérémonie de phase
		 */
	    @GET
	    @Path("/distribute")
	    @Produces(MediaType.APPLICATION_JSON)
	    public Response distribute(@NotNull @QueryParam("token") String token) {
	        return Response.ok()
	                .entity(CeremonyGestion.distribute(token))
	                .build();
	    }
	    
	    /**
		 * Service pour savoir à qui est le tour de parole
		 * @param token	Le token d'un utilisateur
		 * @param tour	(Optionnel) Le tour où l'utilisateur pense être (Permet de ne renvoyer la liste des utilisateurs que si des personnes y ont été ajouté par rapport à la dernière requête)
		 * @return 	status	OK si on a pu récupérer le tour de parole
		 * 			answer 		(contenant "tour" le compteur de tour, "yourturn": true si c'est au tour de l'utilisateur qui a fait la requête et ("fini" : true si tout le monde a parlé ou "de" si il reste au moins un utilisateur qui doit parler. "de" contient "nickname", le nom de l'utilisateur))
		 */
	    @GET
	    @Path("/whosturn")
	    @Produces(MediaType.APPLICATION_JSON)
	    public void whoSTurn(@NotNull @QueryParam("token") final String token, @DefaultValue("-1") @QueryParam("tour") final Integer tour,@Suspended final AsyncResponse asyncResponse) {
	    	new Thread(new Runnable() {
	    		@Override
	    		public void run() {
	    			asyncResponse.resume(Response.ok()
	    	                .entity(CeremonyGestion.whoSTurn(token, tour))
	    	                .build());
	    		}
	    	}).start();
	    }
	    
	    /**
		 * Service pour finir un tour de parole
		 * @param token	Le token d'un utilisateur (ou d'un MDC si byMDC vaut true)
		 * @param tour	Le numéro du tour que la personne veut conclure (Permet de vérifier qu c'est bien le bon tour qui sera conclu)
		 * @param byMDC	Si faux : l'utilisateur veut conclure son propre tour, si vrai, un MDC veut conclure le tour d'un utilisateur
		 * @return 	status	OK si on a pu récupérer le tour de parole
		 */
	    @GET
	    @Path("/turnisover")
	    @Produces(MediaType.APPLICATION_JSON)
	    public Response turnIsOver(@NotNull @QueryParam("token") String token, @NotNull @QueryParam("tour") Integer tour, @DefaultValue("false") @QueryParam("byMDC") Boolean byMdc) {
	        return Response.ok()
	                .entity(CeremonyGestion.turnIsOver(token, tour, byMdc))
	                .build();
	    }
	    
	    /**
	     * Ne pas Implémenter sauf si besoin
	     * @deprecated
	     * @param token
	     * @return
	     */
	    @GET
	    @Path("/close")
	    @Produces(MediaType.APPLICATION_JSON)
	    public Response close(@NotNull @QueryParam("token") String token) {
	        return Response.ok()
	                .entity(ServicesTools.serviceNotYetImplemented("Ce service n'est plus utilisé. La clôture de la cérémonie est automatique"))
	                .build();
	    }
}
