
package teleretro.services;

import javax.validation.constraints.NotNull;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import teleretro.gestionnaire.UserGestion;

/**
 * Root resource (exposed at "utilisateurresource" path)
 */
@Path("/utilisateurresource")
public class UtilisateurServices {
    
	/**
	 * Permet à un utilisateur de se connecter
	 * @param login		mail ou pseudonyme d'un utilisateur
	 * @param password	son mot de passe en clair
	 * @return	status	OK si la connexion est passée
	 * 			token	un token généré aléatoirement, il reste valide 2 heures après l'appel à un service
	 * 			mdc		booléen indiquant si l'utilisateur est un Maître de cérémonie (MDC)
	 */
    @GET
    @Path("/connect")
    @Produces(MediaType.APPLICATION_JSON)
    public Response connect(@NotNull @QueryParam("login") String login, @NotNull  @QueryParam("password") String password) {
    	return Response.ok()
                .entity(UserGestion.connect(login, password))
                .build();
    }
    //http://localhost:8080/utilisateurresource/connect?login=Bob&password=admin123
    
    /**
     * Permet à un utilisateur de se déconnecter
     * @param token	le token qui authentifie l'utilisateur
     * @return	status OK si la déconnexion a bien été effectuée
     */
    @GET
    @Path("/disconnect")
    @Produces(MediaType.APPLICATION_JSON)
    public Response disconnect(@NotNull @QueryParam("token") String token) {
    	return Response.ok()
                .entity(UserGestion.disconnect(token))
                .build();
    }
    
    /**
     * Permet d'ajouter des utilisateurs (ou de réactiver un utilisateur) 
     * @param token	le token du MDC qui ajoute l'utilisateur
     * @param mail	l'adresse mail de l'utilisateur qu'on veut ajouter (ne doit pas déjà être utilisé)
     * @param pseudo	le pseudonyme qu'on veut donner à l'utilisateur
     * @param mdc	un booléen indiquant si l'utilisateur créé est un MDC
     * @return 	status OK si on a pu créer l'utilisateur
     * 			mailsent booléen indiquant si on a pu envoyer un mail à l'utilisateur contenant son mot de passe
     */
    @GET
    @Path("/adduser")
    @Produces(MediaType.APPLICATION_JSON)
    public Response addUser(@NotNull @QueryParam("token") String token, @NotNull @QueryParam("mail") String mail, @NotNull @QueryParam("pseudo") String pseudo, @NotNull @QueryParam("mdc") String mdc ) {
        return Response.ok()
                .entity(UserGestion.addUser(token, mail, pseudo, mdc))
                .build();
    }
    
    /**
     * Permet à un utilisateur de désactiver son propre compte
     * @param token		le token de l'utilisateur
     * @param password	le mot de passe de l'utilisateur
     * @return			status OK si le compte a bien été supprimé
     * 					mailsent booléen indiquant si le mail de notification a bien été envoyé
     */
    @GET
    @Path("/deleteownaccount")
    @Produces(MediaType.APPLICATION_JSON)
    public Response deleteOwnAccount(@NotNull @QueryParam("token") String token, @NotNull @QueryParam("password") String password) {
        return Response.ok()
                .entity(UserGestion.deleteOwnAccount(token, password))
                .build();
    }
    /**
     * Permet à un MDC de désactiver le compte d'un autre utilisateur (ne peut pas être utilisé pour désactiver son propre compte)
     * @param token		le token d'un MDC
     * @param user	l'id de l'utilisateur qu'on veut supprimer
     * @return			status OK si on a pu désactiver le compte
     * 					mailsent booléen indiquant si le mail de notification a bien été envoyé
     */
    @GET
    @Path("/deleteuseraccount")
    @Produces(MediaType.APPLICATION_JSON)
    public Response deleteUserAccount(@NotNull @QueryParam("token") String token, @NotNull @QueryParam("user") Integer user) {
        return Response.ok()
                .entity(UserGestion.deleteUserAccount(token, user))
                .build();
    }
    
    /**
     * Permet à un utilisateur de modifier son mot de passe
     * @param token			le token de l'utilisateur
     * @param oldpassword	son ancien mot de passe
     * @param newpassword	son nouveau mot de passe
     * @return status OK si le mot de passe a bien été modifié
     */
    @GET
    @Path("/changepassword")
    @Produces(MediaType.APPLICATION_JSON)
    public Response changePassword(@NotNull @QueryParam("token") String token, @NotNull @QueryParam("oldpassword") String oldpassword, @NotNull @QueryParam("newpassword") String newpassword ) {
        return Response.ok()
                .entity(UserGestion.changePassword(token, oldpassword, newpassword))
                .build();
    }
    
    /**
     * Permet à un utilisateur de réinitialiser son mot de passe
     * @param login	le login de l'utilisateur
     * @return	status OK si le mot de passe a bien été modifié
     * 			mailsent booléen indiquant si le mail donnant le nouveau mot de passe a bien été envoyé
     */
    @GET
    @Path("/resetpassword")
    @Produces(MediaType.APPLICATION_JSON)
    public Response resetPassword(@NotNull @QueryParam("login") String login ) {
        return Response.ok()
                .entity(UserGestion.resetPassword(login))
                .build();
    }
    
    /**
     * Permet à un mdc de changer le rôle d'un autre utilisateur (on ne peut pas modifier son propre rôle)
     * @param token	le token d'un MDC
     * @param id	l'id de la personne dont on veut changer le rôle
     * @param role	le role qu'on souhaite attribuer à l'utilisateur ("mdc" pour maître de cérémonie ou "user" pour utilisateur standard)
     * @return status OK si on a pu mettre à jour le rôle de l'utilisateur
     */
    @GET
    @Path("/changerole")
    @Produces(MediaType.APPLICATION_JSON)
    public Response changeRole(@NotNull @QueryParam("token") String token, @NotNull @QueryParam("id") int id, @NotNull @QueryParam("role") String role ) {
        return Response.ok()
                .entity(UserGestion.changeRole(token, id, role))
                .build();
    }
    
    /**
     * Permet à un mdc de voir tous les utilisateurs actifs de l'application
     * @param token	le token d'un MDC
     * @return	status 	OK si on pu récupérer la liste
     * 			users	la liste des utilisateurs (contenant 	"id" : l'id de l'utilisateur,
     * 															"nick" : le pseudonyme de l'utilisateur,
     * 															"mail" : l'adresse mail de l'utilisateur,
     * 															"mdc" : "true" si l'utilisateur est un mdc, "false" sinon,
     * 															"wrotepostit" : "true" si une cérémonie est en cours et que l'utilisateur a rempli ses post-its pour celle-ci, "false" sinon)
     */
    @GET
    @Path("/getusers")
    @Produces(MediaType.APPLICATION_JSON)
    public Response getUsers(@NotNull @QueryParam("token") String token) {    	
        return Response.ok()
                .entity(UserGestion.getUsers(token))
                .build();
    }
}
