import { Component, OnInit,Output , ViewChild , AfterViewInit} from '@angular/core';
import { User } from '../modele/User';
import { RegistrationService } from '../services/registration.service';
import { Router } from '@angular/router';
import { MatDialog,MatDialogConfig} from '@angular/material/dialog';
import {RegisterComponent} from '../register/register.component'
import {MatTableDataSource} from '@angular/material/table'
import{UserTable} from '../modele/UserTable'
import {EditRoleComponent} from '../edit-role/edit-role.component'
import {MatPaginator} from '@angular/material/paginator';
import { ConfirmationDialogComponent } from '../confirmation-dialog/confirmation-dialog.component';

const ELEMENT_DATA: UserTable[] = [
  { userName: "", role:"",buttonrole:"",buttonsupp:""},

  
];
@Component({
  selector: 'app-delete-user-account',
  templateUrl: './delete-user-account.component.html',
  styleUrls: ['./delete-user-account.component.css']
})
export class DeleteUserAccountComponent implements OnInit {
  user :User=new User(0,"","","","","","","",false);

  constructor(private _service:RegistrationService,private _root:Router,private dialog:MatDialog) { 
    
  }
  displayedColumns: string[] = [ 'userName', 'role','buttonrole','buttonsupp'];
  dataSource = new MatTableDataSource<UserTable>(ELEMENT_DATA);
 
 // myArray :any= localStorage.getItem("myArray")
 myArray :any= [
 
];


@ViewChild(MatPaginator) paginator: MatPaginator;


ListData :MatDialogConfig<any>


ngOnInit(): void {
  this.fetchdata()
 

}

ngAfterViewInit() {
  this.dataSource.paginator = this.paginator;
}

removeItem(id:number,index :number){
 this._service.deleteUser(id).subscribe(
      
   data=>{
  console.log(data)
     if(data.status=='KO'){
      window.alert(data.message);

     }else{
       console.log(id)
     this.myArray.pop(index)
     console.log(this.myArray.pop(index))
     this.dataSource=this.myArray
     window.location.reload()
     }
      
      },
        error=>{
          console.log("error")
        }
      
      
      )

  }
 

fetchdata(){
  let response=this._service.getAllCollaborateur();
  response.subscribe(
      data=>{
        console.log(data.user)

        for (let i = 0; i <= data.users.length; i++) {

  
          this.myArray.push(data.users.pop(i))
         // this.ListData=new MatTableDataSource(this.myArray)
           this.dataSource=this.myArray
          console.log(this.myArray)
          
        }
        
        
      }
      )

}


  disconnectUser(){
    this._service.disconnect().subscribe(
      data=> {
    
        console.log(data);
        if(data.status == "OK") {
        
        
            this._root.navigate(['']);
         
        }
     
      },
      error=> {
        console.log("exception");
    
      
      }

    )
  }

  onCreate(){
    const dialogConfig = new MatDialogConfig();
    dialogConfig.disableClose=false;
    //dialogConfig.autoFocus=true;
    dialogConfig.width="60%";
    dialogConfig.height="80%";
    this.dialog.open(RegisterComponent,dialogConfig);
}

  onCreateRole(id:number){
    localStorage.setItem('id',String(id));
    const dialogConfig = new MatDialogConfig();
    dialogConfig.disableClose=true;
    dialogConfig.autoFocus=true;
    dialogConfig.width="60%";
    dialogConfig.height="80%";
    this.dialog.open(EditRoleComponent,dialogConfig);


  }

  openDeleteDialog(id:number, index:number): void {
    const dialogRef = this.dialog.open(ConfirmationDialogComponent, {
      width: '350px',
      data: "Êtes-vous sûr(e) de vouloir supprimer cet utilisateur ?"
    });
    dialogRef.afterClosed().subscribe(result => {
      if(result) {
        console.log('Yes clicked');
        this.removeItem(id, index);
      }
    });
  }

  homeRedirection(){
    if (Boolean(localStorage.getItem("isMdc")) == true) {
      this._root.navigate(['/ceremony_master']);
    }
    else {
      this._root.navigate(['/user']);
    }
  }


}
