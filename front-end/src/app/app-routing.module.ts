import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LoginComponent } from './login/login.component';
import { CeremonyMasterComponent} from './ceremony-master/ceremony-master.component';
import { UserComponent } from './user/user.component';
import { ProfileComponent } from './profile/profile.component';
import { DeleteUserAccountComponent } from './delete-user-account/delete-user-account.component';
import { CeremonyHistoryComponent } from './ceremony-history/ceremony-history.component';
import { RegisterComponent } from './register/register.component';
import { ResetPasswordComponent } from './reset-password/reset-password.component';
import { DeletMyCountComponent }  from './delet-my-account/delet-my-count.component';
import { CeremonyUserViewComponent } from './ceremony-user-view/ceremony-user-view.component';
import { CeremonyMdcViewComponent } from './ceremony-mdc-view/ceremony-mdc-view.component';
import { AddDeadlineComponent } from './add-deadline/add-deadline.component';
import { EditRoleComponent } from './edit-role/edit-role.component';
import { PostItsComponent } from './post-its/post-its.component';
import {ForgotPasswordComponent} from './forgot-password/forgot-password.component'
import { ModifyDeadlineComponent } from './modify-deadline/modify-deadline.component';

const routes: Routes = [
  {path:'', component: LoginComponent},
  {path:'ceremony_master', component: CeremonyMasterComponent},
  {path:'user', component: UserComponent},
  {path:'profile', component: ProfileComponent},
  {path:'user_account', component: DeleteUserAccountComponent},
  {path:'ceremony_history', component: CeremonyHistoryComponent},
  {path:'register', component: RegisterComponent},
  {path:'edit_password', component: ResetPasswordComponent},
  {path:'delete_my_account', component: DeletMyCountComponent},
  {path:'ceremony_user_view', component: CeremonyUserViewComponent},
  {path:'ceremony_mdc_view', component: CeremonyMdcViewComponent},
  {path:'add_deadline', component: AddDeadlineComponent},
  {path:'postits', component: PostItsComponent},
  {path:'forgot_password', component: ForgotPasswordComponent}
  
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
