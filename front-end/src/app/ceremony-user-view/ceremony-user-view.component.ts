import { Component, OnInit,Input } from '@angular/core';



import {RegistrationService} from '../services/registration.service';
import {User } from '../modele/User'
import {CeremonyService}  from '../services/ceremony.service'
import {Ceremony} from '../modele/Ceremony'
import {InteractionCeremonyService} from '../services/interactionCeremony.service'
import { Router } from '@angular/router';
import {PostitService} from '../services/postit.service'
import {Postits} from '../modele/Postits'
import * as DataTable from '../data-table/data-table-datasource';
import { MatDialog } from '@angular/material/dialog';
import { ConfirmationDialogComponent } from '../confirmation-dialog/confirmation-dialog.component';
@Component({
  selector: 'app-ceremony-user-view',
 
  templateUrl: './ceremony-user-view.component.html',
  styleUrls: ['./ceremony-user-view.component.css']
})
export class CeremonyUserViewComponent implements OnInit {

  click : boolean = true;
  postits :Postits =new Postits("","");
  user :User=new User(0,"","","","","","","",false);
  ceremony :Ceremony=new Ceremony(0,"","","","","");
  count:number=0;
  mytour : Boolean=false;
  userTour:string="";
  @Input () userTourr 
  msg:any=""
 
  // myArray :any= localStorage.getItem("myArray")
  myArray :any= [
  
 ];
 myArrayPostits :any= [
  
];
 
 permission :boolean=false;
 tour : number =-1;

 constructor(private _service : RegistrationService,
  private _serviceCeremony :CeremonyService,
  private _root:Router,
  private _servicepost : PostitService,
  private _interactcerem: InteractionCeremonyService,
  public dialog: MatDialog) { }
 
 ngOnInit(): void {

console.log(this.userTourr)
 this.getParticipant()

 

 this.whosTurn()


   let respone2=this._servicepost.getpostitcerem().subscribe(
    data=> {

      if(data.status=="OK"){
       
        if(data.postIts.length!=0){
         console.log(data)
           for (let i = 0; i <= data.postIts.length; i++) {
             this.myArrayPostits.push(data.postIts.pop(i).content)
            
               
           }
           this.postits.positif=this.myArrayPostits.pop(0)
           this.postits.negatif=this.myArrayPostits.pop(1)
     
         }

      }else{
       // window.alert(data.message)
      }
 
   
    },
    error=> {
      console.log("exception");

    }

  )
 
    
      
}

whosTurn(){
    
  let reponse1=this._serviceCeremony.whosturn().subscribe(
    data=> {
   
   if(data.status=="OK"){
    this.tour=data.answer.tour;
    localStorage.setItem('tour',String(this.tour))
    console.log(data)
  this.mytour=data.answer.yourturn;
  
  if(this.mytour){
    this.click = false;
    this.msg=this.tour
  this.userTourr=data.answer.de.nickname
  console.log(data.answer.de.nickname)

  
    this._serviceCeremony.whosturnwith(this.tour).subscribe(
      data=>{
        console.log(data)
        if(data.answer.fini=true){
        
          this._root.navigate(['/user']);
        
        }else{
          this.userTourr=data.answer.de.nickname
        this._interactcerem.updateData(this.userTourr)
         
        }

      }

    )
   }else{
    this.click = true;
    this.userTourr=data.answer.de.nickname
   // this._root.navigate(['/ceremony_master']);
   }

  }else{
   
    this.userTourr="C'est le tour de l'utilisateur"+':'+data.answer.de.nickname
   this.userTourr=data.answer.de.nickname
    this.msg=this.tour
  }
   
    },
    error=> {
      console.log("exception");

    }
  )
}

turnisover(){
  this.permission=true;
        localStorage.setItem('permission',String(this.permission));
  this._serviceCeremony.turnisover(this.tour).subscribe(
    
      data=> {
        if(data.status=="OK"){
          console.log(data)
        // this.tour=data.answer.tour
         
     
      }else{
      //  window.alert("vo")

      }
       
      },
      error=> {
        console.log("exception");
      
      
      }

  )
}



getParticipant(){
  let response=this._serviceCeremony.getParticipant();

  response.subscribe(
      data=>{
   
       if(data.participants != null){
        this.myArray = [];
        for (let i=0; i<= data.participants.length; i++) {
          this.myArray.push(data.participants.pop(i).nickname)
 
      

        }

        
      }else{
        window.alert("il n'existe pas de participants pour l'instant")
      }

      }
      )
}




passerSonTour(){
  this._serviceCeremony.whosturn().subscribe(
    data=> {
   
    //  console.log(data.answer.tour)


    },
    error=> {
      console.log("exception");

    }
  )
 
}

exitceremony(){
  if(localStorage.getItem("permission") == "true") {

    this._root.navigate(['/user']);


  }
}
disconnectUser(){
    this._service.disconnect().subscribe(
      data=> {
    
        console.log(data);
        if(data.status == "OK") {
        
        
            this._root.navigate(['']);
         
        }
     
      },
      error=> {
        console.log("exception");
     
      
      }

    )
  }

  homeRedirection(){
    if (Boolean(localStorage.getItem("isMdc")) == true) {
      this._root.navigate(['/ceremony_master']);
    }
    else {
      this._root.navigate(['/user']);
    }
  }

  openOwnTurnDialog(): void {
    const dialogRef = this.dialog.open(ConfirmationDialogComponent, {
      width: '350px',
      data: "Êtes-vous sûr(e) de vouloir finir votre tour ?"
    });
    dialogRef.afterClosed().subscribe(result => {
      if(result) {
        console.log('Yes clicked');
        this.turnisover();
      }
    });
  }

}
