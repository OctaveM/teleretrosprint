import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CeremonyUserViewComponent } from './ceremony-user-view.component';

describe('CeremonyUserViewComponent', () => {
  let component: CeremonyUserViewComponent;
  let fixture: ComponentFixture<CeremonyUserViewComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CeremonyUserViewComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CeremonyUserViewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
