import { Component, OnInit } from '@angular/core';
import {RegistrationService} from '../services/registration.service'
import { Router } from '@angular/router';
import{UserTable} from '../modele/UserTable'
import {MatTableDataSource} from '@angular/material/table'
import { CeremonyService }  from '../services/ceremony.service'


const ELEMENT_DATA: UserTable[] = [
  { userName: "", role:"",buttonrole:"",buttonsupp:""},

  
];

@Component({
  selector: 'app-ceremony-history',
  templateUrl: './ceremony-history.component.html',
  styleUrls: ['./ceremony-history.component.css']
})
export class CeremonyHistoryComponent implements OnInit {
  displayedColumns: string[] = [ 'ceremonyName','dateStart'];
  dataSource = new MatTableDataSource<UserTable>(ELEMENT_DATA);

  myArray :any= [
 
  ];
  constructor(private _serviceuser :RegistrationService,private _root:Router,private _serviceCeremony :CeremonyService) { }

  ngOnInit(): void {
    this._serviceCeremony.consulthistory().subscribe(
      data=> {
    
        console.log(data);
        if(data.status == "OK") {
          for (let i = 0; i <= data.ceremonies.length; i++) {

  
            this.myArray.push(data.ceremonies.pop(i))
           // this.ListData=new MatTableDataSource(this.myArray)
             this.dataSource=this.myArray
            console.log(data.ceremonies.pop(0))
            
          }
  
         
        }else{
          window.alert(data.message)
        }
     
      },
      error=> {
        console.log("exception");
       
      
      }
  
    )
   
  }


  consulthistory(){
    this._serviceCeremony.consulthistory().subscribe(
      data=> {
    
        console.log(data);
        if(data.status == "OK") {
          for (let i = 0; i <= data.ceremonies.length; i++) {

  
            this.myArray.push(data.ceremonies.pop(i))
           // this.ListData=new MatTableDataSource(this.myArray)
             this.dataSource=this.myArray
            console.log(data.ceremonies.pop(0))
            
          }
    
         
        }else{
          window.alert(data.message)
        }
     
      },
      error=> {
        console.log("exception");
       
      
      }
  
    )
  
  }
  disconnectUser(){
    this._serviceuser.disconnect().subscribe(
      data=> {
    
        console.log(data);
        if(data.status == "OK") {
         
        
            this._root.navigate(['']);
         
        }
     
      },
      error=> {
        console.log("exception");
      
      
      }

    )
  }

  homeRedirection(){
    if (Boolean(localStorage.getItem("isMdc")) == true) {
      this._root.navigate(['/ceremony_master']);
    }
    else {
      this._root.navigate(['/user']);
    }
  }
}
