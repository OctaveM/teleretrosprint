import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CeremonyHistoryComponent } from './ceremony-history.component';

describe('CeremonyHistoryComponent', () => {
  let component: CeremonyHistoryComponent;
  let fixture: ComponentFixture<CeremonyHistoryComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CeremonyHistoryComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CeremonyHistoryComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
