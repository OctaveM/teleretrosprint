import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import {FormsModule} from '@angular/forms'
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { LoginComponent } from './login/login.component';
import { FlexLayoutModule } from '@angular/flex-layout';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatInputModule } from '@angular/material/input';
import { MatButtonModule } from '@angular/material/button';
import { MatCardModule } from '@angular/material/card';
import { MatToolbarModule } from '@angular/material/toolbar';
import { MatIconModule } from '@angular/material/icon';
import { MatListModule } from '@angular/material/list';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { CeremonyMasterComponent } from './ceremony-master/ceremony-master.component';
import { UserComponent } from './user/user.component';
import { ProfileComponent } from './profile/profile.component';
import { DeleteUserAccountComponent } from './delete-user-account/delete-user-account.component';
import { CeremonyHistoryComponent } from './ceremony-history/ceremony-history.component';
import {  HttpClientModule } from '@angular/common/http';
import { RegisterComponent } from './register/register.component';
import { ConfirmValidationDirective } from 'src/app/validator/confirm-equal-validator.directive';
import { ResetPasswordComponent } from './reset-password/reset-password.component';
import { DeletMyCountComponent } from './delet-my-account/delet-my-count.component';
import { MatRadioModule } from '@angular/material/radio';
import { MatSelectModule } from '@angular/material/select';
import { CeremonyUserViewComponent } from './ceremony-user-view/ceremony-user-view.component';
import { DataTableComponent } from './data-table/data-table.component';
import { MatTableModule } from '@angular/material/table';
import { MatPaginatorModule } from '@angular/material/paginator';
import { MatSortModule } from '@angular/material/sort';
import { CeremonyMdcViewComponent } from './ceremony-mdc-view/ceremony-mdc-view.component';
import { AddDeadlineComponent } from './add-deadline/add-deadline.component';
import {MatDialogModule } from '@angular/material/dialog';
import { AddUsersFormComponent } from './add-users-form/add-users-form.component';
import { EditRoleComponent } from './edit-role/edit-role.component';
import {CookieService} from 'ngx-cookie-service';
import { ForgotPasswordComponent } from './forgot-password/forgot-password.component';
import { PostItsComponent } from './post-its/post-its.component';
import { ConfirmationDialogComponent } from './confirmation-dialog/confirmation-dialog.component';
import { ModifyDeadlineComponent } from './modify-deadline/modify-deadline.component';

@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    CeremonyMasterComponent,
    UserComponent,
    ProfileComponent,
    DeleteUserAccountComponent,
    CeremonyHistoryComponent,
    RegisterComponent,
    ConfirmValidationDirective,
    ResetPasswordComponent,
    DeletMyCountComponent,
    CeremonyUserViewComponent,
    DataTableComponent,
    CeremonyMdcViewComponent,
    AddDeadlineComponent,
    AddUsersFormComponent,
    EditRoleComponent,
    ForgotPasswordComponent,
    PostItsComponent,
    ConfirmationDialogComponent,
    ModifyDeadlineComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FlexLayoutModule,
    MatFormFieldModule,
    MatInputModule,
    MatButtonModule,
    MatCardModule,
    MatToolbarModule,
    MatIconModule,
    MatListModule,
    MatSelectModule,
    MatRadioModule,
    FormsModule,
    BrowserAnimationsModule,
    HttpClientModule,
    MatTableModule,
    MatPaginatorModule,
    MatSortModule,
    MatDialogModule,
  ],
  providers: [CookieService],
  bootstrap: [AppComponent],
  entryComponents:[RegisterComponent]
})
export class AppModule { 


  myArrayapp=[]
}
