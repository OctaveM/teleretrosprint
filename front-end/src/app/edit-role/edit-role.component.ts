import { Component, OnInit } from '@angular/core';
import {User} from '../modele/User'
import { RegistrationService } from '../services/registration.service';
import { Router } from '@angular/router';
import { MatDialog,MatDialogConfig} from '@angular/material/dialog';
import {RegisterComponent} from '../register/register.component';
import {MatTableDataSource} from '@angular/material/table';
import{UserTable} from '../modele/UserTable';
import {MatDialogRef} from '@angular/material/dialog';
import { ConfirmationDialogComponent } from '../confirmation-dialog/confirmation-dialog.component';

@Component({
  selector: 'app-edit-role',
  templateUrl: './edit-role.component.html',
  styleUrls: ['./edit-role.component.css']
})
export class EditRoleComponent implements OnInit {
  user:User=new User(0,"","","","","","","",false);
 
  constructor(private _service:RegistrationService,private _root:Router,private dialog:MatDialog,public dialogRef : MatDialogRef<RegisterComponent>) { 
    dialogRef.disableClose = true;
  }

  ngOnInit(): void {
  }
  onClose(){
    this.dialogRef.close();
  }
   
  editRole(){
    
    if(Boolean(this.user.rolemdc)==true){
      this.user.role="mdc"

    }else if(Boolean(this.user.roleutilisateur)==true){
      this.user.role="user"
    }
   
    this._service.modifyRole(Number(localStorage.getItem('id')),this.user).subscribe(
         
      data=>{
        console.log(this.user.role)
        if(data.status == 'OK'){
          window.alert(' La modification du rôle  est faite avec succès.')
          this.onClose()
        }else{
          window.alert(data.message)
          this.onClose()
        }
      
      
         
         },
           error=>{
            this.onClose()
             console.log("error")
           }
         
         
         )
   
     }

     openEditDialog(): void {
      const dialogRef = this.dialog.open(ConfirmationDialogComponent, {
        width: '350px',
        data: "Êtes-vous sûr(e) de vouloir modifier le rôle de cet utilisateur ?"
      });
      dialogRef.afterClosed().subscribe(result => {
        if(result) {
          console.log('Yes clicked');
          this.editRole();
        }
      });
    }

     closeDialog() {
      this.dialog.closeAll();
    }

    homeRedirection(){
      if (Boolean(localStorage.getItem("isMdc")) == true) {
        this._root.navigate(['/ceremony_master']);
      }
      else {
        this._root.navigate(['/user']);
      }
    }

}
