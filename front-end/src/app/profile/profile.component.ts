import { Component, OnInit } from '@angular/core';
import { RegistrationService } from '../services/registration.service';
import { Router } from '@angular/router';


@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.css']
})
export class ProfileComponent implements OnInit {


  users:any;
  constructor(private _service:RegistrationService,private _root:Router) { }

  ngOnInit(): void {
  }
  /*removeItem(username:string){
    let resp=this._service.delete(username);
    //resp.subscribe((data)=>this.users=data)


  }*/

  
  disconnectUser(){
    this._service.disconnect().subscribe(
      data=> {
    
        console.log(data);
        if(data.status == "OK") {
        
        
            this._root.navigate(['']);
         
        }
     
      },
      error=> {
        console.log("exception");

      
      }

    )
  }

  homeRedirection(){
    if (Boolean(localStorage.getItem("isMdc")) == true) {
      this._root.navigate(['/ceremony_master']);
    }
    else {
      this._root.navigate(['/user']);
    }
  }
  
}
