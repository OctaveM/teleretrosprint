import { Component, AfterViewInit,OnInit, ViewChild } from '@angular/core';
import { Router } from '@angular/router';
import { User } from '../modele/User';
import { RegistrationService } from '../services/registration.service';
import {MatPaginator} from '@angular/material/paginator';
import { MatDialog } from '@angular/material/dialog';
import { ConfirmationDialogComponent } from '../confirmation-dialog/confirmation-dialog.component';

@Component({
  selector: 'app-delet-my-count',
  templateUrl: './delet-my-count.component.html',
  styleUrls: ['./delet-my-count.component.css']
})
export class DeletMyCountComponent implements OnInit {

user:User=new User(0,"","","","","","","",false);
  constructor(private _service:RegistrationService,private _root:Router, public dialog: MatDialog) { }



 
  ngOnInit(): void {
    
  }


  deleteMyAccount(){
  
    this._service.deleteMyAccount(this.user).subscribe(

      data=> {
       // console.log(data)
      if(data.status=="OK"){
       
        
       this._root.navigate(['']);
      }else{
        window.alert(data.message)
        console.log(data)
      }
         
   
      },
      error=> {
        console.log("exception");
        console.log(localStorage.getItem("token"));
        console.log(this.user.password);
       
      
      }

    )}

    disconnectUser(){
      this._service.disconnect().subscribe(
        data=> {
      
          console.log(data);
          if(data.status == "OK") {
          
          
              this._root.navigate(['']);
           
          }
       
        },
        error=> {
          console.log("exception");
     
        
        }
  
      )
    }

    openDialog(): void {
      const dialogRef = this.dialog.open(ConfirmationDialogComponent, {
        width: '350px',
        data: "Êtes-vous sûr(e) de vouloir supprimer votre compte ?"
      });
      dialogRef.afterClosed().subscribe(result => {
        if(result) {
          console.log('Yes clicked');
          this.deleteMyAccount();
        }
      });
    }

    homeRedirection(){
      if (Boolean(localStorage.getItem("isMdc")) == true) {
        this._root.navigate(['/ceremony_master']);
      }
      else {
        this._root.navigate(['/user']);
      }
    }

}
