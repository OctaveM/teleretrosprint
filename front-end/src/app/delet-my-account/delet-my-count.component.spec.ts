import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DeletMyCountComponent } from './delet-my-count.component';

describe('DeletMyCountComponent', () => {
  let component: DeletMyCountComponent;
  let fixture: ComponentFixture<DeletMyCountComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ DeletMyCountComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(DeletMyCountComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
