import { Component, OnInit } from '@angular/core';
import { User } from '../modele/User';
import { RegistrationService } from '../services/registration.service';
import {NgForm} from '@angular/forms';
import { Router } from '@angular/router';
import { MatDialog,MatDialogConfig} from '@angular/material/dialog';
import {MatDialogRef} from '@angular/material/dialog';
import { ConfirmationDialogComponent } from '../confirmation-dialog/confirmation-dialog.component';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {
user :User=new User(0,"","","","","","","",false);
msg:any="";
  constructor(private _service:RegistrationService
    ,private _root:Router,private dialog:MatDialog,
    public dialogRef : MatDialogRef<RegisterComponent>) {
      dialogRef.disableClose = true;
     }

  ngOnInit(): void {

  }

   RegisterNow(){
    this._service.doRegistration(this.user).subscribe(
    data=>{
console.log(data)
      if(data.status=="OK"){
      
        this.onClose()
      this._root.navigate(['/delete_user_account']);
      window.location.reload()

      }else  if(data.status ="KO"){
        window.alert(data.message)
        this.onClose()
      }


      },
      error=>{
        this.msg = "L'inscription a échoué."
        console.log(this.user.role)
        
      }
    )
  }

onClose(){
  this.dialogRef.close();
}

openDialog(): void {
  const dialogRef = this.dialog.open(ConfirmationDialogComponent, {
    width: '350px',
    data: "Êtes-vous sûr(e) de vouloir inscrire cet utilisateur ?"
  });
  dialogRef.afterClosed().subscribe(result => {
    if(result) {
      console.log('Yes clicked');
      this.RegisterNow();
    }
  });
}

closeDialog() {
  this.dialog.closeAll();
}

homeRedirection(){
  if (Boolean(localStorage.getItem("isMdc")) == true) {
    this._root.navigate(['/ceremony_master']);
  }
  else {
    this._root.navigate(['/user']);
  }
}
 

}
