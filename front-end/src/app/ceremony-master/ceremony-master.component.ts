import { Component, OnInit } from '@angular/core';
import { User } from '../modele/User';
import { RegistrationService } from '../services/registration.service';
import { CeremonyService }  from '../services/ceremony.service'
import { Ceremony } from '../modele/Ceremony'
import { Router } from '@angular/router';
import { MatDialog } from '@angular/material/dialog';
import { PostitService} from '../services/postit.service';
import { Postits } from '../modele/Postits';
import { ConfirmationDialogComponent } from '../confirmation-dialog/confirmation-dialog.component';
@Component({
  selector: 'app-ceremony-master',
  templateUrl: './ceremony-master.component.html',
  styleUrls: ['./ceremony-master.component.css']
})
export class CeremonyMasterComponent implements OnInit {
  user:User=new User(0,"","","","","","","",false);
  ceremony :Ceremony=new Ceremony(0,"","","","","");
  msg:any="";
  postits :Postits =new Postits("","");
  insertpostit :Boolean=false;

  constructor(private _service:RegistrationService,private _serviceCeremony :CeremonyService,private _root:Router, public dialog: MatDialog, private _servicepost : PostitService) { }

  ngOnInit(): void {
  
  }

  myArray :any= [

    { id:1, userName:"Utilisateur1"},
 
  ];


 getCollaborateur(){
   this._service.getAllCollaborateur().subscribe(
     data=>{
       console.log(data)
       console.log(localStorage.getItem("token"))
     this.myArray.push(data)
     console.log(this.myArray)
       //localStorage.setItem("myArray",this.myArray)

     },
     error=>{
       console.log("error")
     }
   )

 }
participate(){
  this._serviceCeremony.participate(this.ceremony).subscribe(
    data=>{
    if(data.participants != null){
      for (let i = 0; i <= data.participants.length; i++) {

        this.myArray.push(data.participants.pop(i).nickname)

      }
    }else{
     // window.alert("il n'existe pas de participants pour l'instant")
    }
  }
  )
}
   
 lancerCeremony(){
  this._serviceCeremony.lancerCeremonyService().subscribe(
    data=> {
      if(data.status == "OK") {
        console.log(data)
       this.participate()
       this._root.navigate(['/ceremony_mdc_view']);
      
      }
      else if(data.status=="KO") {
        window.alert(data.message)
       
      }
    },
    error=> {
      console.log("exception");
     
    
    }

  )
}

getOwnCurrent() {
 
  this._servicepost.getOwnCurrent().subscribe(
    data=> {


      if(data.status == "OK") {
        this.insertpostit = true;
        this.postits.positif = data.positif;
        this.postits.negatif = data.negatif;

      }else{
        this.msg = data.message;
        console.log(this.msg);
      }
   console.log(this.insertpostit);
    },
    error=> {
      console.log("exception");
    
    
    }

  )
}


defineCeremony(){
  this._serviceCeremony.defineCeremony(this.ceremony).subscribe(
    data=> {
      
     
      if(data.status == "OK") {
      
   
       console.log("ok")
      }
      else  if(data.status == "KO"){
      window.alert(data.message)
       
      }
    },
    error=> {
      console.log("exception");
     
    
    }

  )
}

disconnectUser(){
  this._service.disconnect().subscribe(
    data=> {
  
      console.log(data);
      if(data.status == "OK") {
      
      
          this._root.navigate(['']);
       
      }
   
    },
    error=> {
      console.log("exception");
    
    
    }

  )
}

consulthistory(){
  this._serviceCeremony.consulthistory().subscribe(
    data=> {
  
      console.log(data);
      if(data.status == "OK") {
        console.log("iciiiiiiiii")

       console.log( data.ceremonies.pop(0))
       
      }else{
        window.alert(data.message)
      }
   
    },
    error=> {
      console.log("exception");
     
    
    }

  )

}

openDialog(): void {
  const dialogRef = this.dialog.open(ConfirmationDialogComponent, {
    width: '350px',
    data: "Êtes-vous sûr(e) de vouloir lancer la cérémonie ?"
  });
  dialogRef.afterClosed().subscribe(result => {
    if(result) {
      console.log('Yes clicked');
     // this.lancerCeremony();
    }
  });
}

homeRedirection(){
  if (Boolean(localStorage.getItem("isMdc")) == true) {
    this._root.navigate(['/ceremony_master']);
  }
  else {
    this._root.navigate(['/user']);
  }
}

}
