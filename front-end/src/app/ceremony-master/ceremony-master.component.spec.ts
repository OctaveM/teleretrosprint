import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CeremonyMasterComponent } from './ceremony-master.component';

describe('CeremonyMasterComponent', () => {
  let component: CeremonyMasterComponent;
  let fixture: ComponentFixture<CeremonyMasterComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CeremonyMasterComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CeremonyMasterComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
