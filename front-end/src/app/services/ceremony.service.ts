import { Injectable } from '@angular/core';
import {User} from '../modele/User'
import {Ceremony } from '../modele/Ceremony'
import { HttpClient, HttpParams, HttpHeaders } from '@angular/common/http';

import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class CeremonyService {
  ceremony:Ceremony=new Ceremony(0,"","","","","")

  constructor(private _http :HttpClient) { }

 

  public lancerCeremonyService( ):Observable<any> {
    let params = new HttpParams().set("token",localStorage.getItem('token'));
    
   return this._http.get<any>('http://localhost:8080/ceremonyresource/start',{params:params});
  }

  
  public getParticipant():Observable<any> {
    let params = new HttpParams().set("token",localStorage.getItem('token'));
    
   return this._http.get<any>('http://localhost:8080/ceremonyresource/getparticipants',{params:params});
  }
 
 
  public distribute():Observable<any> {
    let params = new HttpParams().set("token",localStorage.getItem('token'));
    
   return this._http.get<any>('http://localhost:8080/ceremonyresource/distribute',{params:params});
  }

  public defineCeremony( ceremony:Ceremony):Observable<any> {
 
    let params = new HttpParams().append("token",localStorage.getItem('token')).append("name",ceremony.ceremonyName).append("date",ceremony.deadline);
    

   return this._http.get<any>('http://localhost:8080/ceremonyresource/definenewceremony',{params:params });

 

  }

  public participate( ceremony:Ceremony):Observable<any> {
    let params = new HttpParams().set("token",localStorage.getItem('token'));
    
   return this._http.get<any>('http://localhost:8080/ceremonyresource/participate',{params:params});
  }

  public participateCount( count:number):Observable<any> {
    let params = new HttpParams().set("token",localStorage.getItem('token'));
    
   return this._http.get<any>("http://localhost:8080/ceremonyresource/getparticipants?token="+localStorage.getItem("token")+"&tour="+count);
  }



  public turnisover (tour:number ):Observable<any> {
    //let params = new HttpParams().set("token",localStorage.getItem('token')).set("tour",tour);
    
   return this._http.get<any>("http://localhost:8080/ceremonyresource/turnisover?token="+localStorage.getItem("token")+"&tour="+tour);
  }

  public turnisoverBymdc (tour:number ):Observable<any> {
    let byMdc:Boolean =true;
    
    //let params = new HttpParams().set("token",localStorage.getItem('token')).set("tour",tour);
    
   return this._http.get<any>("http://localhost:8080/ceremonyresource/turnisover?token="+localStorage.getItem("token")+"&tour="+tour+"&byMDC="+byMdc);
  }


  public closeCeremony ( ):Observable<any> {
    let params = new HttpParams().set("token",localStorage.getItem('token'));
    
   return this._http.get<any>('http://localhost:8080/ceremonyresource/close',{params:params});
  }

  public whosturn():Observable<any> {
    let params = new HttpParams().set("token",localStorage.getItem('token'));
    
   return this._http.get<any>('http://localhost:8080/ceremonyresource/whosturn',{params:params});
  }

  public whosturnwith( tour:number):Observable<any> {
    let params = new HttpParams().set("token",localStorage.getItem('token'));
    
   return this._http.get<any>("http://localhost:8080/ceremonyresource/whosturn?token="+localStorage.getItem("token")+"&tour="+tour);
  }

  public consulthistory():Observable<any> {
    let params = new HttpParams().set("token",localStorage.getItem('token'));
    
   return this._http.get<any>('http://localhost:8080/ceremonyresource/consulthistory',{params:params});
  }

  public getCurrentCeremony() :Observable<any>  {
    let params = new HttpParams().set("token", localStorage.getItem("token"));
    return this._http.get<any>("http://localhost:8080/utilisateurresource/getcurrentcerem",{params:params});

  }

 
 

  
}
