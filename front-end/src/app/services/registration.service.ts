import { HttpClient, HttpParams, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import {User} from '../modele/User'
@Injectable({
  providedIn: 'root'
})
export class RegistrationService {
  msg:any;

  constructor(private _http :HttpClient) { 
    this.msg="";
  }

  public loginUserFromRemote(user:User ):Observable<any> {
    let params = new HttpParams().set("login",user.email).set("password",user.password);
    
   return this._http.get<any>('http://localhost:8080/utilisateurresource/connect',{params:params});
  }


  public disconnect():Observable<any> {
    let params = new HttpParams().set("token",localStorage.getItem('token'));
    
   return this._http.get<any>('http://localhost:8080/utilisateurresource/disconnect',{params:params});
  }


  public doRegistration(user:User):Observable<any>  {
    let params = new HttpParams().set("token", localStorage.getItem("token")).set("mail",user.email).set("pseudo",user.userName).set("mdc",user.role);
    return this._http.get<any>('http://localhost:8080/utilisateurresource/adduser',{params:params});

  }

  public getAllCollaborateur() :Observable<any>  {
    let params = new HttpParams().set("token", localStorage.getItem("token"));
    return this._http.get<any>("http://localhost:8080/utilisateurresource/getusers",{params:params});

  }

  public getCollaborateurByEmail(email:any) {

     return this._http.get("url"+email);
  }
 

  public deleteUser(id:number) :Observable<any> {
    let options = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
      }),
      body: {
        user:id ,
        token: localStorage.getItem("token"),
      },
    };
  
    let params = new HttpParams().set("token",localStorage.getItem("token"));
    const url= "http://localhost:8080/utilisateurresource/deleteuseraccount?token="+localStorage.getItem("token")+"&user="+id
  
  return this._http.get(url)
  }


  public modifyRole(id:number,user:User) :Observable<any> {

    
    let options = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
      }),
      body: {
        user:id ,
        token: localStorage.getItem("token"),
      },
    };
   
    let params = new HttpParams().set("token",localStorage.getItem("token"));
    const url= "http://localhost:8080/utilisateurresource/changerole?token="+localStorage.getItem("token")+"&id="+id+"&role="+user.role

  return this._http.get(url)
  }

  public deleteMyAccount(user:User):Observable<any>  {
    let params = new HttpParams().set("token",localStorage.getItem("token")).set("password",user.password);
    return this._http.get<any>('http://localhost:8080/utilisateurresource/deleteownaccount',{params:params});
    
  }

  public resetPassword(user:User):Observable<any>  {
    let params = new HttpParams().set("token",localStorage.getItem("token")).set("oldpassword",user.password).set("newpassword",user.newpassword);
    return this._http.get<any>('http://localhost:8080/utilisateurresource/changepassword',{params:params});
    
  }

  public forgotPassword(user:User):Observable<any>  {
    let params = new HttpParams().set("login",user.email)
    return this._http.get<any>('http://localhost:8080/utilisateurresource/resetpassword',{params:params});
    
  }



}
