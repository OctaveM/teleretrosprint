import { Injectable } from '@angular/core';
import {User} from '../modele/User'
import {Ceremony } from '../modele/Ceremony'
import {Postits} from '../modele/Postits'
import { HttpClient, HttpParams, HttpHeaders } from '@angular/common/http';

import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class PostitService {
    constructor(private _http :HttpClient) { }


    public getpostitcerem ( ):Observable<any> {
        let params = new HttpParams().set("token",localStorage.getItem('token'));
        
       return this._http.get<any>('http://localhost:8080/postitresource/getpostitcerem',{params:params});
      }

      public addpostits(postits:Postits):Observable<any> {
        let params = new HttpParams().set("token",localStorage.getItem('token')).set("positif",postits.positif).set("negatif",postits.negatif);
        
       return this._http.post<any>('http://localhost:8080/postitresource/insert',"",{params:params});
      }

      public updatepostits(postits:Postits):Observable<any> {
        let params = new HttpParams().set("token",localStorage.getItem('token')).set("positif",postits.positif).set("negatif",postits.negatif);
        
       return this._http.post<any>('http://localhost:8080/postitresource/update',"",{params:params});
      }

      public getOwnCurrent():Observable<any> {
        let params = new HttpParams().set("token",localStorage.getItem('token'));
        
       return this._http.get<any>('http://localhost:8080/postitresource/getOwnCurrent',{params:params});
      }
}