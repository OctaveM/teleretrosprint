import { Injectable } from '@angular/core';
import {User} from '../modele/User'
import {Ceremony } from '../modele/Ceremony'
import {Postits} from '../modele/Postits'
import{BehaviorSubject} from "rxjs";
import { HttpClient, HttpParams, HttpHeaders } from '@angular/common/http';

import { Observable, Subject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class InteractionCeremonyService {

  private content=new BehaviorSubject<string>("")
  public share=this.content.asObservable();
 constructor(){

 }

 updateData(text){
   this.content.next(text);

 }

}