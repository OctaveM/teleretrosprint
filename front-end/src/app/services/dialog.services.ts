import{Injectable}  from '@angular/core'
import {MatDialog} from '@angular/material/dialog'
import { DeleteUserAccountComponent} from '../delete-user-account/delete-user-account.component'


@Injectable({
    providedIn:'root'
})

export class DialogService{


    constructor(private dialog:MatDialog){

    }

    openConfirmDialog(){
        this.dialog.open(DeleteUserAccountComponent,{
            width: '390px',
            disableClose :true
        })
    }
}