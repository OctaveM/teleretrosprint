import { Component, OnInit } from '@angular/core';
import { User } from '../modele/User';
import { RegistrationService } from '../services/registration.service';
import { Router } from '@angular/router';
import {LoginComponent} from '../login/login.component';
import { MatDialog } from '@angular/material/dialog';
import { ConfirmationDialogComponent } from '../confirmation-dialog/confirmation-dialog.component';

@Component({
  selector: 'app-reset-password',
  templateUrl: './reset-password.component.html',
  styleUrls: ['./reset-password.component.css']
})
export class ResetPasswordComponent implements OnInit {
  user:User=new User(0,"","","","","","","",false)
  msg:any="";
  constructor(private _service:RegistrationService,private _root:Router, public dialog: MatDialog) { }


  resetPassword(){
    this._service.resetPassword(this.user).subscribe(
      data=> {
      
        this.msg = data.message;
        if(data.status == "OK") {
          
          this._root.navigate(['']);
          
        }
        
      
      },
      error=> {
        console.log("exception");
       
      
      }

    )
  }

  ngOnInit(): void {
  }
  disconnectUser(){
    this._service.disconnect().subscribe(
      data=> {
    
        console.log(data);
        if(data.status == "OK") {
        
        
            this._root.navigate(['']);
         
        }
     
      },
      error=> {
        console.log("exception");
        this.msg="login error";
      
      }

    )
  }

  openDialog(): void {
    const dialogRef = this.dialog.open(ConfirmationDialogComponent, {
      width: '350px',
      data: "Êtes-vous sûr(e) de vouloir modifier votre mot de passe ?"
    });
    dialogRef.afterClosed().subscribe(result => {
      if(result) {
        console.log('Yes clicked');
        this.resetPassword();
      }
    });
  }

  homeRedirection(){
    if (Boolean(localStorage.getItem("isMdc")) == true) {
      this._root.navigate(['/ceremony_master']);
    }
    else {
      this._root.navigate(['/user']);
    }
  }
  
}
