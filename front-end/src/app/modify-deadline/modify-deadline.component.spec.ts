import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ModifyDeadlineComponent } from './modify-deadline.component';

describe('ModifyDeadlineComponent', () => {
  let component: ModifyDeadlineComponent;
  let fixture: ComponentFixture<ModifyDeadlineComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ModifyDeadlineComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ModifyDeadlineComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
