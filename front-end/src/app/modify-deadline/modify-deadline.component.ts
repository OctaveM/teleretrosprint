import { Component, OnInit,Output , ViewChild , AfterViewInit} from '@angular/core';
import { User } from '../modele/User';
import { CeremonyService } from '../services/ceremony.service';
import { RegistrationService } from '../services/registration.service';
import { Router } from '@angular/router';
import { MatDialog,MatDialogConfig} from '@angular/material/dialog';
import {RegisterComponent} from '../register/register.component'
import {MatTableDataSource} from '@angular/material/table'
import{CeremonyTable} from '../modele/CeremonyTable'
import {EditRoleComponent} from '../edit-role/edit-role.component'
import {MatPaginator} from '@angular/material/paginator';
import { ConfirmationDialogComponent } from '../confirmation-dialog/confirmation-dialog.component';

const ELEMENT_DATA: CeremonyTable[] = [
  { idCeremony: 0, nameCeremony:"",deadlineCeremony:"",startdate:""},

  
];

@Component({
  selector: 'app-modify-deadline',
  templateUrl: './modify-deadline.component.html',
  styleUrls: ['./modify-deadline.component.css']
})
export class ModifyDeadlineComponent implements OnInit {

  user :User=new User(0,"","","","","","","",false);

  constructor(private _service:CeremonyService, private _registrationService:RegistrationService,private _root:Router,private dialog:MatDialog) { 
    
  }
  displayedColumns: string[] = [ 'idCeremony', 'nameCeremony','deadlineCeremony','startdate'];
  dataSource = new MatTableDataSource<CeremonyTable>(ELEMENT_DATA);
 
  // myArray :any= localStorage.getItem("myArray")
  myArray :any= [
  
  ];


  @ViewChild(MatPaginator) paginator: MatPaginator;


  ListData :MatDialogConfig<any>

  ngOnInit(): void {
    this.fetchdata()
   
  
  }
  
  ngAfterViewInit() {
    this.dataSource.paginator = this.paginator;
  }

  fetchdata(){
    let response=this._service.getCurrentCeremony();
    response.subscribe(
        data=>{
          console.log(data)
  
          /*for (let i = 0; i <= data.users.length; i++) {
  
    
            this.myArray.push(data.users.pop(i))
           // this.ListData=new MatTableDataSource(this.myArray)
             this.dataSource=this.myArray
            console.log(this.myArray)
            
          }*/
          
          
        }
        )
  
  }
  
  
    disconnectUser(){
      this._registrationService.disconnect().subscribe(
        data=> {
      
          console.log(data);
          if(data.status == "OK") {
          
          
              this._root.navigate(['']);
           
          }
       
        },
        error=> {
          console.log("exception");
      
        
        }
  
      )
    }

    homeRedirection(){
      if (Boolean(localStorage.getItem("isMdc")) == true) {
        this._root.navigate(['/ceremony_master']);
      }
      else {
        this._root.navigate(['/user']);
      }
    }

}
