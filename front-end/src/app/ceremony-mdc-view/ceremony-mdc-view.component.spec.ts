import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CeremonyMdcViewComponent } from './ceremony-mdc-view.component';

describe('CeremonyMdcViewComponent', () => {
  let component: CeremonyMdcViewComponent;
  let fixture: ComponentFixture<CeremonyMdcViewComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CeremonyMdcViewComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CeremonyMdcViewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
