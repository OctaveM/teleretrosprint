import { Component, Input, OnInit } from '@angular/core';
import {RegistrationService} from '../services/registration.service';
import {User } from '../modele/User'
import {CeremonyService}  from '../services/ceremony.service'
import {Ceremony} from '../modele/Ceremony'
import { Router } from '@angular/router';
import {UserComponent} from '../user/user.component'
import {PostitService} from '../services/postit.service'
import {Postits} from '../modele/Postits'
import * as DataTable from '../data-table/data-table-datasource';
import {CeremonyUserViewComponent} from '../ceremony-user-view/ceremony-user-view.component'
import {InteractionCeremonyService} from '../services/interactionCeremony.service';
import { MatDialog } from '@angular/material/dialog';
import { ConfirmationDialogComponent } from '../confirmation-dialog/confirmation-dialog.component';


@Component({
  selector: 'app-ceremony-mdc-view',
  template:`<app-ceremony-user-view > </app-ceremony-user-view>`,

  templateUrl: './ceremony-mdc-view.component.html',
  styleUrls: ['./ceremony-mdc-view.component.css']
})
export class CeremonyMdcViewComponent implements OnInit {

  click : boolean = true;
  user_click : boolean = true;
  distribute_click : boolean = false;
  postits :Postits =new Postits("","");
  user :User=new User(0,"","","","","","","",false);
  ceremony :Ceremony=new Ceremony(0,"","","","","");
  msg:any=""
 
  // myArray :any= localStorage.getItem("myArray")
  myArray :any= [];
  myArrayParticipant :any= [];
  myArrayPostits :any= [ ];
  tour : number =-1;
  mytour : Boolean=false;
 permission :boolean=false;
 count :number=0
  mdc=true;
  userTourr:string="";
 private userView:CeremonyUserViewComponent
 

  constructor(private _service : RegistrationService,
    private _serviceCeremony :CeremonyService,
    private _root:Router,
    private _servicepost : PostitService,
    private _interactcerem: InteractionCeremonyService,
    public dialog: MatDialog
    ) { }

  
  ngOnInit(): void {
   this.getParticipant();

       this.whosTurn();
          


    let respone3=this._servicepost.getpostitcerem().subscribe(
      data=> {
        if(data.status=="OK"){
          console.log(data)
          if(data.postIts.length>=0){
            for (let i = 0; i <= data.postIts.length; i++) {
              this.myArrayPostits.push(data.postIts.pop(i).content)
           
             
      
            }
            this.postits.positif=this.myArrayPostits.pop(0)
            this.postits.negatif=this.myArrayPostits.pop(1)
          }

        }else{
          window.alert(data.message)
        }
      
  
  
      },
      error=> {
        console.log("exception");
  
      }
  
    )

  }

 

  closeCeremony(){
    this._serviceCeremony.closeCeremony().subscribe(
      data=>{
        window.alert(data.status)
      }
    
  )
  }

  whosTurn(){
    
    let reponse1=this._serviceCeremony.whosturn().subscribe(
      data=> {
     
     if(data.status=="OK"){
      this.tour=data.answer.tour;
      localStorage.setItem('tour',String(this.tour))
      console.log(data)
    this.mytour=data.answer.yourturn;
    
    if(this.mytour){
      this.click = false;
      this.user_click = true;
      this.msg=this.tour
     this.userTourr=data.answer.de.nickname
      this._serviceCeremony.whosturnwith(this.tour).subscribe(
        data=>{
          if(data.answer.de!=undefined){
          console.log(data)
          this.userTourr=data.answer.de.nickname
          this._interactcerem.updateData(this.userTourr)
         
          
          }else if(data.answer.fini==true){
            this._root.navigate(['/ceremony_master']);
          }
  
        }
  
      )
     }else{
     
     }
  
    }else{
      this.click = true;
      this.user_click = false;
      this.userTourr="C'est le tour de l'utilisateur"+':'+data.answer.de.nickname
     this.userTourr=data.answer.de.nickname
      this.msg=this.tour
    }
     
  
        
  
      },
      error=> {
        console.log("exception");
  
      }
    )
  }
  getParticipant(){
    let response=this._serviceCeremony.getParticipant();
  
    response.subscribe(
        data=>{
     
         if(data.participants != null){
          this.myArray = [];
          for (let i = 0; i <= data.participants.length; i++) {
            this.myArray.push(data.participants.pop(i).nickname)
            console.log(this.myArray)
            

            this.count++;
        

          }
          
        }else{
          //window.alert("il n'existe pas de participants pour l'instant")
        }
 
        }
        )
  }


  realodPage(){
    
    window.location.reload();
 }
 distribute(){
  this.distribute_click = true;
  if(this.myArray==null){
    window.alert("Aucun participant!")

  }else{
   
  this._serviceCeremony.distribute().subscribe(
      data=> {
     
        if(data.status == "OK") {
        window.alert("les post-its sont distribués .")
        }
        else {
          
          console.log("ko")
        
        }
      },
      error=> {
        console.log("exception");
      
      
      }

  )
    }
}

passturnuser(){
  if( !this.mytour){
    let response3=this._serviceCeremony.whosturnwith(this.tour).subscribe(
      data=>{
        window.alert("passer")
      }
    )

  }
}
turnisover(){
  this.permission=true;
        localStorage.setItem('permission',String(this.permission));
  this._serviceCeremony.turnisover(this.tour).subscribe(
    
      data=> {
        if(data.satuts=="OK"){
          console.log(data)
          this.whosTurn()

        }else{
         window.alert(data.message)
        }
       
          
                  
      },
      error=> {
        console.log("exception");
      
      
      }

  )
}


exitceremony(){
  if(localStorage.getItem("permission") == "true") {

    this._root.navigate(['/ceremony_master']);

  }

}

getpostitceremony(){
 
  this._servicepost.getpostitcerem().subscribe(
    data=> {
   
      for (let i = 0; i <= data.postIts.length; i++) {
        this.myArrayPostits.push(data.postIts.pop(i).content)
       

      }
      console.log('hvvhh')
      console.log(data.postIts)
      this.postits.positif=this.myArrayPostits.pop(0)
      this.postits.negatif=this.myArrayPostits.pop(1)


    },
    error=> {
      console.log("exception");

    }

  )

}

participate(){

 
}

paticipatecerem(){
  this.participate()
  let response=this._serviceCeremony.getParticipant();
 response.subscribe(
        data=>{
          console.log(data)
         if(data.participants != null){
          for (let i = 0; i <= data.participants.length; i++) {
            this.myArray.push(data.participants.pop(i))
          //  console.log(this.myArray)
          
            
          }
        }else{
          window.alert("il n'existe pas de participants pour l'instant")
        }
          
          
        }
        )
}

passerSonTour(){
  this._serviceCeremony.whosturn().subscribe(
    data=> {
   
      console.log(data)


    },
    error=> {
      console.log("exception");

    }
  )
}

disconnectUser(){
  this._service.disconnect().subscribe(
    data=> {
  
      console.log(data);
      if(data.status == "OK") {
      
      
          this._root.navigate(['']);
       
      }
   
    },
    error=> {
      console.log("exception");
    
    
    }

  )
}

homeRedirection(){
  if (Boolean(localStorage.getItem("isMdc")) == true) {
    this._root.navigate(['/ceremony_master']);
  }
  else {
    this._root.navigate(['/user']);
  }
}

openDistributeDialog(): void {
  const dialogRef = this.dialog.open(ConfirmationDialogComponent, {
    width: '350px',
    data: "Êtes-vous sûr(e) de vouloir distribuer les post-its ?"
  });
  dialogRef.afterClosed().subscribe(result => {
    if(result) {
      console.log('Yes clicked');
      this.distribute();
    }
  });
}

openOwnTurnDialog(): void {
  const dialogRef = this.dialog.open(ConfirmationDialogComponent, {
    width: '350px',
    data: "Êtes-vous sûr(e) de vouloir finir votre tour ?"
  });
  dialogRef.afterClosed().subscribe(result => {
    if(result) {
      console.log('Yes clicked');
      this.turnisover();
    }
  });
}

openUserTurnDialog(): void {
  const dialogRef = this.dialog.open(ConfirmationDialogComponent, {
    width: '350px',
    data: "Êtes-vous sûr(e) de vouloir finir le tour de l'utilisateur auquel c'est le tour ?"
  });
  dialogRef.afterClosed().subscribe(result => {
    if(result) {
      console.log('Yes clicked');
      this.turnisover();
    }
  });
}

}
