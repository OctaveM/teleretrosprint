import { Component, OnInit } from '@angular/core';
import { Ceremony } from '../modele/Ceremony'
import { CeremonyService } from '../services/ceremony.service'
import { RegistrationService } from '../services/registration.service'
import { Router } from '@angular/router';

import { PostitService } from '../services/postit.service'
import { Postits } from '../modele/Postits'
import { User } from '../modele/User';

import { MatDialog } from '@angular/material/dialog';
import { ConfirmationDialogComponent } from '../confirmation-dialog/confirmation-dialog.component';


@Component({
  selector: 'app-user',
  templateUrl: './user.component.html',
  styleUrls: ['./user.component.css']
})
export class UserComponent implements OnInit {
  ceremony :Ceremony=new Ceremony(0,"","","","","");
  
  msg:any="";



  postits :Postits =new Postits("","");
  insertpostit :Boolean=false;

  constructor( private _service:CeremonyService,private _serviceuser :RegistrationService,private _root:Router, public dialog: MatDialog, private _servicepost : PostitService) { }


  ngOnInit(): void {
    this.getOwnCurrent()
  }

  getpostitceremony(){
 
    this._servicepost.getpostitcerem().subscribe(
      data=> {
     
        console.log(data.postIts.pop(0).content)


      },
      error=> {
        console.log("exception");

      }

    )

  }

getOwnCurrent() {
 
    this._servicepost.getOwnCurrent().subscribe(
      data=> {
  
  
        if(data.status == "OK") {
          this.insertpostit=true;
          this.postits.positif=data.positif;
          this.postits.negatif=data.negatif;

        }else{
          this.msg=data.message;
          console.log(this.msg);
        }
     console.log(this.insertpostit)
      },
      error=> {
        console.log("exception");
      
      
      }
  
    )
  }



  participate(){
  
    this._service.participate(this.ceremony).subscribe(
      data=> {
        console.log(data)
        if(data.status=="OK"){
    
          this.getpostitceremony()
         console.log(data)
          this._root.navigate(['/ceremony_user_view']);

      }else{
          window.alert(data.message.concat('' ,this.msg))
          //console.log(data)
        }
          
       
     
      },
      error=> {
        console.log("exception");
        
     
      
      }

    )
  }
  disconnectUser(){
    this._serviceuser.disconnect().subscribe(
      data=> {
    
        console.log(data);
        if(data.status == "OK") {
        
        
            this._root.navigate(['']);
         
        }
     
      },
      error=> {
        console.log("exception");
       
      
      }

    )
  }

 

  openDialog(): void {
    const dialogRef = this.dialog.open(ConfirmationDialogComponent, {
      width: '350px',
      data: "Êtes-vous sûr(e) de vouloir participer à la cérémonie ?"
    });
    dialogRef.afterClosed().subscribe(result => {
      if(result) {
        console.log('Yes clicked');
        this.participate();
      }
    });
  }

  homeRedirection(){
    if (Boolean(localStorage.getItem("isMdc")) == true) {
      this._root.navigate(['/ceremony_master']);
    }
    else {
      this._root.navigate(['/user']);
    }
  }
  

}
