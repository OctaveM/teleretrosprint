import { Component, OnInit } from '@angular/core';
import {Ceremony} from '../modele/Ceremony'
import {CeremonyService} from '../services/ceremony.service';
import {RegistrationService} from '../services/registration.service'
import { Router } from '@angular/router';

import {NgForm} from '@angular/forms';

import { MatDialog } from '@angular/material/dialog';
import { ConfirmationDialogComponent } from '../confirmation-dialog/confirmation-dialog.component';


@Component({
  selector: 'app-add-deadline',
  templateUrl: './add-deadline.component.html',
  styleUrls: ['./add-deadline.component.css']
})
export class AddDeadlineComponent implements OnInit {
  msg;any="";

  ceremony:Ceremony = new Ceremony(0,"","","","","")
  constructor(private _service :CeremonyService,private _serviceuser :RegistrationService,private _root:Router, public dialog: MatDialog) { }

  ngOnInit(): void {

  }

  defineCeremony(){
    this.ceremony.deadline = this.ceremony.endDate + " " + this.ceremony.endTime + ".0";
   
    this._service.defineCeremony(this.ceremony).subscribe(
      data=> {
        console.log(data)
    
        if(data.status== 'OK' ){
          window.alert("ceremony créee")
          window.location.reload()
        }else{
          
          window.alert(data.message)
          window.location.reload()
        }
     
      },
      error=> {
        console.log("exception");
        
        this.msg="login error";
      
      }

    )
  }


  disconnectUser(){
    this._serviceuser.disconnect().subscribe(
      data=> {
    
        console.log(data);
        if(data.status == "OK") {
        
        
            this._root.navigate(['']);
         
        }
     
      },
      error=> {
        console.log("exception");
      
      }

    )
  }

  openDialog(): void {
    const dialogRef = this.dialog.open(ConfirmationDialogComponent, {
      width: '350px',
      data: "Êtes-vous sûr(e) de vouloir ajouter cette deadline ?"
    });
    dialogRef.afterClosed().subscribe(result => {
      if(result) {
        console.log('Yes clicked');
        this.defineCeremony();
      }
    });
  }

  homeRedirection(){
    if (Boolean(localStorage.getItem("isMdc")) == true) {
      this._root.navigate(['/ceremony_master']);
    }
    else {
      this._root.navigate(['/user']);
    }
  }
  

}
