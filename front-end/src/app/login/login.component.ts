import { Component, OnInit } from '@angular/core';
import {NgForm} from '@angular/forms';
import { Router } from '@angular/router';
import { CookieService } from 'ngx-cookie-service';
import { User } from '../modele/User';
import { RegistrationService } from '../services/registration.service';


@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
user:User=new User(0,"","","","","","","",false);
msg:any="";
login :boolean=false;
  constructor(private _service:RegistrationService,private _root:Router,private cookie:CookieService) {

   }


  

  ngOnInit(): void {
    if(Boolean(localStorage.getItem('isLogin'))==true){
      this.user.email=localStorage.getItem('email')
      this.user.password=localStorage.getItem('password')

    }
  }
  loginUser(){
    this._service.loginUserFromRemote(this.user).subscribe(
      data=> {
     localStorage.setItem('token',data.token)
     localStorage.setItem('isMdc',String(data.mdc))
        if(data.status == "OK") {
          this.msg = "La connexion a été établie avec succès.";
          if(data.mdc) {
            this._root.navigate(['/ceremony_master']);
          }
          else {
            this._root.navigate(['/user']);
          }
        }
        else {
          console.log(data)
          this.msg = "La connexion a échoué.";
        }
      },
      error=> {
        console.log("exception");
        this.msg="login error";
      
      }

    )
  }
  islogin(){

    if(this.login==false){
      this.login=true
      localStorage.setItem('isLogin',String(this.login))
      
 
      localStorage.setItem('username',this.user.userName)
      localStorage.setItem('email',this.user.email)
      localStorage.setItem('password',this.user.password)
      console.log(this.user.password)
    
      
     window.alert("session enregistrée")
  
  

    }

  }




}
