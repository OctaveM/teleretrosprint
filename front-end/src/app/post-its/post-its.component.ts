import { Component, OnInit } from '@angular/core';

import {RegistrationService} from '../services/registration.service';
import {User } from '../modele/User'
import {CeremonyService}  from '../services/ceremony.service'
import {Ceremony} from '../modele/Ceremony'
import { Router } from '@angular/router';
import {Postits} from '../modele/Postits'
import {PostitService} from '../services/postit.service'

import { MatDialog } from '@angular/material/dialog';
import { ConfirmationDialogComponent } from '../confirmation-dialog/confirmation-dialog.component';


@Component({
  selector: 'app-post-its',
  templateUrl: './post-its.component.html',
  styleUrls: ['./post-its.component.css']
})
export class PostItsComponent implements OnInit {

  constructor(private _service : RegistrationService,private _serviceCeremony :CeremonyService,private _root:Router,private _servicepostits : PostitService,public dialog: MatDialog) { }
  postits :Postits =new Postits("","");
  insertpostit :Boolean=true;
  user:User = new User(0,"","","","","","","",false)
  msg:any=""



  ngOnInit(): void {
this.getOwnCurrent()
    
  }
  getOwnCurrent(){
    this._servicepostits.getOwnCurrent().subscribe(
      data=> {
    
        console.log(data);
        if(data.status == "OK") {
          this.insertpostit=true;
          this.postits.positif=data.positif
          this.postits.negatif=data.negatif
        
        
         
        }else{
        
          this.msg=data.message
          console.log(this.msg)
        }
     
      },
      error=> {
        console.log("exception");
      
      
      }
  
    )
  }

  disconnectUser(){

   
    this._service.disconnect().subscribe(

      data=> {
    
        console.log(data);
        if(data.status == "OK") {
        
        
            this._root.navigate(['']);
         
        }
     
      },
      error=> {
        console.log("exception");

      
      
      }
  
    )
  }

modifypostits(){
  this._servicepostits.updatepostits(this.postits).subscribe(
    data=>{
      if(data.status=="OK"){
      window.location.reload()
     //localStorage.setItem('positif',this.postits.positif)
    //localStorage.setItem('negatif',this.postits.negatif)
     console.log("update")
      console.log(data)
      window.alert("Le contenu des deux post-its est modifié.")
      }else{
        window.alert(data.message +'.'+"Veuillez ajouter des post-its pour pouvoir les modifier.")

      }
      
    }
  )
}
 
  addpostits(){
    
    this._servicepostits.addpostits(this.postits).subscribe(
      data=> {
        if(data.status=='OK' ){
         // localStorage.setItem('positif',this.postits.positif)
         // localStorage.setItem('negatif',this.postits.negatif)
         if(this.getOwnCurrent()==null){
          window.alert("Post-its ajoutés avec succès.")
          this.user.postit=true;
         }
        


        }else if(data.status=="KO") {
          
            window.alert(data.message +" Vous ne pouvez pas ajouter de post-its.")
         
  
        }
        
     
      },
      error=> {
        console.log("exception");
   
      
      }

    )
  }

       
  

  openAddDialog(): void {
    const dialogRef = this.dialog.open(ConfirmationDialogComponent, {
      width: '350px',
      data: "Êtes-vous sûr(e) de vouloir ajouter ces post-its ?"
    });
    dialogRef.afterClosed().subscribe(result => {
      if(result) {
        console.log('Yes clicked');
        this.addpostits();
      }
    });
  }

  openEditDialog(): void {
    const dialogRef = this.dialog.open(ConfirmationDialogComponent, {
      width: '350px',
      data: "Êtes-vous sûr(e) de vouloir enregistrer vos modifications ?"
    });
    dialogRef.afterClosed().subscribe(result => {
      if(result) {
        console.log('Yes clicked');
        this.modifypostits();
      }
    });
  }

  homeRedirection(){
    if (Boolean(localStorage.getItem("isMdc")) == true) {
      this._root.navigate(['/ceremony_master']);
    }
    else {
      this._root.navigate(['/user']);
    }
  }


}
