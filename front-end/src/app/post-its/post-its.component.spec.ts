import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PostItsComponent } from './post-its.component';

describe('PostItsComponent', () => {
  let component: PostItsComponent;
  let fixture: ComponentFixture<PostItsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ PostItsComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(PostItsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
