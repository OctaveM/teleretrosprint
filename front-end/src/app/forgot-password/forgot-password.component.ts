import { Component, OnInit } from '@angular/core';
import { User } from '../modele/User';
import { RegistrationService } from '../services/registration.service';
import { Router } from '@angular/router';
import {LoginComponent} from '../login/login.component'
@Component({
  selector: 'app-forgot-password',
  templateUrl: './forgot-password.component.html',
  styleUrls: ['./forgot-password.component.css']
})
export class ForgotPasswordComponent implements OnInit {


  ngOnInit(): void {
  }
  user:User=new User(0,"","","","","","","",false)
  msg:any="";
  constructor(private _service:RegistrationService,private _root:Router) { }


  resetPassword(){
    console.log("icccii")
    this._service.forgotPassword(this.user).subscribe(
      
      data=> {

        if(data.status=="OK"){

          if(localStorage.getItem('password')!=null){
            localStorage.removeItem('password')
            localStorage.setItem('password',this.user.password)
          }
          
        console.log(data)
    
            this._root.navigate(['']);
            window.alert(" Mot de pass modifié avec succès.")

        }else{
          window.alert(data.message)
        }
    
        
      
      },
      error=> {
        console.log("exception");
       
      
      }

    )
  }


  disconnectUser(){
    this._service.disconnect().subscribe(
      data=> {
    
        console.log(data);
        if(data.status == "OK") {
        
        
            this._root.navigate(['']);
         
        }
     
      },
      error=> {
        console.log("exception");
        this.msg="login error";
      
      }

    )
  }

  homeRedirection(){
    if (Boolean(localStorage.getItem("isMdc")) == true) {
      this._root.navigate(['/ceremony_master']);
    }
    else {
      this._root.navigate(['/user']);
    }
  }
}
