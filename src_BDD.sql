CREATE DATABASE IF NOT EXISTS teleretro;
CREATE USER 'admin'@'%' IDENTIFIED BY 'admin';
GRANT ALL PRIVILEGES ON teleretro.* TO 'admin'@'%' WITH GRANT OPTION;
USE teleretro;

DROP TABLE IF EXISTS session_saver;
DROP TABLE IF EXISTS post_it;
DROP TABLE IF EXISTS ceremony;
DROP TABLE IF EXISTS user; #suppression en dernier à cause des FK

CREATE TABLE user (user_id INTEGER PRIMARY KEY AUTO_INCREMENT,
				user_nickname VARCHAR(32) UNIQUE NOT NULL,
				user_mail VARCHAR(64) UNIQUE NOT NULL,
				user_password VARCHAR(40) NOT NULL,
				user_salt VARCHAR(5) NOT NULL,
				user_ceremony_master BOOLEAN,
				user_activated BOOLEAN );
				
CREATE TABLE session_saver (session_key VARCHAR(36) PRIMARY KEY,
				user_id INTEGER UNIQUE,
				session_date TIMESTAMP NOT NULL,
				CONSTRAINT user_exists_session FOREIGN KEY (user_id) REFERENCES user(user_id) ON DELETE CASCADE);
						
CREATE TABLE ceremony (ceremony_id INTEGER PRIMARY KEY AUTO_INCREMENT,
				ceremony_name VARCHAR(32) UNIQUE NOT NULL,
				#date de lancement de la cérémonie
				ceremony_start_date TIMESTAMP, 
				#date limite de rendu des cérémonies
				ceremony_deadline TIMESTAMP NOT NULL,
				ceremony_is_over BOOLEAN);
				
CREATE TABLE post_it		(post_it_id INTEGER PRIMARY KEY AUTO_INCREMENT,
				user_id INTEGER NOT NULL,
				ceremony_id INTEGER NOT NULL,
				type BOOLEAN,
				content VARCHAR(1024) NOT NULL,
				CONSTRAINT user_exists_post_it FOREIGN KEY (user_id) REFERENCES user(user_id),
				CONSTRAINT ceremony_exists_post_it FOREIGN KEY (ceremony_id) REFERENCES ceremony(ceremony_id));

INSERT INTO user (user_nickname, user_mail, user_password, user_salt, user_ceremony_master, user_activated) VALUES ("first_user", "first@none.com", "38749f15026868e827264913fef1a0340eb50bd8","salut", 1, 1); #son mot de passe est toujours "admin123"

#sauvegarde
#mysqldump -u root -p -d teleretro > ma_bd.sql

#charger
#mysql -u root -p
#create db / USE / source ma_bd.sql
